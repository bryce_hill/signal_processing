README

## Created January, 2016
## Software Defined Radio Communication Code (ECEN 5002 Class Repository)
## Author: Bryce Hill

### ------- Purpose  : -------

Signal Processing & Software Defined Radio (SDR) Library:

This repository was created as a class repository for ECEN 5002. However, it
contains very useful python and GNU Radio files for use in communications
signal processing and software defined radio. The repository is currently
broken into Lab_# folders. However, all python code is contained in the
my_library directory and can be installed in python2 and 3 python using the
development mode by runnning the install_bryce_lib.sh script and uninstalled
using the corresponding uninstall_bryce_lib.sh script.

### ------- Directory : -------


#### my_library:
my_am

- Amplitude modulation transmitter and receiver

my_ascbin

- Contains various ascci and binary conversion files

my_ft

- Contains basic fourier transform and eye diagram code TODO: eye diagram fits in SDR

my_grc

- Contains code specifically used with GNU Radio

my_jupyter

- Contains formatting code to be used in jupyter notebook

my_shiftKeying

- Amplitude and Frequncy shift keying tramsitters and receivers

my_sdr

- Contains the bulk of the code. sdr stands for Software Defined Radio

my_sinecos

- Some basic sinc and rcf pulses are defined here via functions


#### Docs:
Assorted coursework files

#### Lab_#:
Each lab contains .grc GNU Radio files as well as .pynb jupyter notebook files

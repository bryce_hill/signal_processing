# File: showfun.py
# "show" functions like showft, showspd, etc
from pylab import *
import numpy as np

def showft(sig_xt, ff_lim, feature):
	"""
	Plot (DFT/FFT approximation to) Fourier transform of
	waveform x(t). Displays magnitude |X(f)| either linear
	and absolute or normalized (wrt to maximum value) in dB.
	Phase of X(f) is shown in degrees.
	>>>>> showft(sig_xt, ff_lim) <<<<<
	where	sig_xt:				waveform from class sigWave
			sig_xt.timeAxis():	time axis for x(t)
			sig_xt.signal():	sampled CT signal x(t)
			ff_lim = 	[f1, f2, llim]
			f1:			lower frequency limit for display
			f2:			upper frequency limit for display
			llim = 0:	dispay |X(f)| linear and absolute
			llim > 0:	same as llim =0 but phase is maksed
						(set to zero) for |X(f)|< llim
			llim < 0:	display 20*log_{10}(|X(f)|/max(|X(f)|)
						in dB with lower display limit llim dB,
						phase is masked (set to zero) for f
						with magnitude (dB, normalized) < llim
			feature:	= 0 | 1 | 2 depending on the amound of
						features wanted. To complete this lab
						I had to implement something like this
						or have 3 different showft functions.
						I have to always run the whole ipython
						script from scratch when I change external
						files so the method described in the description
						using the "reload" feature doesn't work.
	"""
	# ***** Preamble/housekeeping *****
	# Define f1, f2, and llim for ease of readability
	f1 = float(ff_lim[0])
	f2 = float(ff_lim[1])
	llim = float(ff_lim[2])
	# ***** Prepare x(t), swap pos/neg parts of time axis *****
	N = len(sig_xt)					# Blocklength of DFT/FFT
	Fs = sig_xt.get_Fs()			# Sampling rate
	tt = sig_xt.timeAxis()			# Get time axis for x(t)
	ixp = where(tt>=0)[0]			# Indices for t >= 0
	ixn = where(tt<0)[0]			# Indices for t < 0
	xt = sig_xt.signal()			# Get x(t)
	xt = hstack((xt[ixp], xt[ixn]))	# Swap pos/neg time axis

	# ***** Compute X(f), make frequency axis *****
	Xf = fft(xt)/float(Fs)			# DFT/FFT of x(t)
									# Scaled for X(f) approximation
	ff = Fs*array(arange(N),int64)/float(N) 	# Frequency axis

	# ***** Compute |X(f)|, arg[X(f)] *****
		# ****
	print("f1: ", f1, "| f2: ", f2, " | llim: ", llim)
	if feature == 0:
		absXf = abs(Xf)					# Magnitude |X(f)|
		argXf = angle(Xf)				# Phase arg[X(f)]
	if feature > 0:
		if ff_lim[0] < 0:
			ixn_seg2 = ff >= int(Fs/2)
			ixp_seg1 = ff < int(Fs/2)
			ff = np.concatenate((ff[ixn_seg2]-Fs, ff[ixp_seg1]))
			Xf = np.concatenate((Xf[ixn_seg2], Xf[ixp_seg1]))
		absXf = abs(Xf)					# Magnitude |X(f)|
		argXf = angle(Xf)				# Phase arg[X(f)]

	if feature == 2:
		absXf = 20*log10(abs(Xf)/max(abs(Xf)))	# Magnitude |X(f)|
		argXf = angle(Xf)						# Phase arg[X(f)]

		# **** Process X(f) for showing (1.(a).ii) ****
	if feature == 1:
		if float(llim) > float(0): # Mask to zero any X(f) < llim
			low_value_indices = absXf < llim
			argXf[low_value_indices] = 0
	elif feature == 2:
		# if float(pow(10,llim/20)) > float(0): # Mask to zero any X(f) < llim
		if float(llim) < float(0):
			low_value_indices = absXf < llim
			argXf[low_value_indices] = 0

	# ***** Plot magnitude/phase *****
	f1 = figure(figsize = [15,7])
	af11 = f1.add_subplot(211)
	af11.plot(ff,absXf)			# Plot magnitude
	af11.grid()
		# **** Set Axis Labels ****
	if feature < 2:
		af11.set_ylabel('|X(f)|')
	elif feature == 2:
		af11.set_ylabel('|X(f)| [dB]')
	strgt = 'FT Approximation, $F_s=$' + str(Fs) + ' Hz'
	strgt = strgt + ', N=' + str(N)
	strgt = strgt + ', $\Delta_f$={0:3.2f}'.format(Fs/float(N)) + ' Hz'
	af11.set_title(strgt, size = 30)
	af12 = f1.add_subplot(212)
	af12.plot(ff,180/pi*argXf) 	# Plot phase in degrees
	af12.grid()
	af12.set_ylabel('arg[X(f)] [deg]')
	af12.set_xlabel('f [Hz]')

		# **** Set Axis Limits ****
	if feature > 0:
		af11.set_xlim(ff_lim[0], ff_lim[1])
		af12.set_xlim(ff_lim[0], ff_lim[1])

	if feature == 2:
		af11.set_ylim(llim, 0)

	show()

# File: pamfun.py
# Functions for pulse amplitude modulation (PAM)
from pylab import *
import ecen4652 as ecen
import os
# Get imports from Lab_1 dir
os.chdir("/home/bryce/Documents/Class/Signal_Processing/Lab_1/My_Functions")
import ascfun
import sinc_ipol
os.chdir("/home/bryce/Documents/Class/Signal_Processing/Lab_2")

def pam10(sig_an, Fs, ptype, polarity, pparms=[]):
	"""
	Pulse amplitude modulation: a_n -> s(t), -TB/2<=t<(N-1/2)*TB,
	V1.0 for 'rect', 'sinc', and 'tri' pulse types.
	>>>>> sig_st = pam10(sig_an, Fs, ptype, pparms) <<<<<
	where 	sig_an: 			sequence from class sigSequ
			sig_an.signal(): 	N-symbol DT input sequence a_n, 0 <= n < N
			sig_an.get_FB(): 	Baud rate of a_n, TB=1/FB
			Fs:					sampling rate of s(t)
			ptype: 				pulse type ('rect','sinc','tri')
			pparms:				pparms not used for 'rect','tri'
								pparms = [k, beta] for 'sinc'
			polarity:			display as unipolar or polar
			k:					"tail" truncation parameter for 'sinc'
								(truncates p(t) to -k*TB <= t < k*TB)
			beta: 				Kaiser window parameter for 'sinc'
			sig_st: 			waveform from class sigWave
			sig_st.timeAxis(): 	time axis for s(t), starts at -TB/2
			sig_st.signal():	CT output signal s(t), -TB/2<=t<(N-1/2)*TB,
								with sampling rate Fs
	"""
	# string = ascfun.bin2asc(sig_an.signal())

	N = len(sig_an)			# Number of data symbols
	FB = sig_an.get_FB()	# Baud rate
	n0 = sig_an.get_n0()	# Starting index
	ixL = int(ceil(-Fs*(n0+0.5)/float(FB)))	# Left index for time axis
	ixR = int(ceil(Fs*(n0+N-0.5)/float(FB))) # Right index for time axis
	tt = arange(ixL,ixR)/float(Fs) 		# Time axis for s(t)
	t0 = tt[0]				# Start time for s(t)

	# ***** Conversion from DT a_n to CT a_s(t) *****
	an = sig_an.signal()		# Sequence a_n
	tan = arange(0, len(an))/float(FB)
	# print("len(an): ", len(an), 'tan: ', tan)
	ast = zeros(len(tt))		# Initialize a_s(t)
	ix = array(around(Fs*arange(0,N)/float(FB)),int)	# Symbol center indexes
	ast[ix-int(ixL)] = Fs*an	# delta_n -> delta(t) conversion

	# ***** Set up PAM pulse p(t) *****
	ptype = ptype.lower()		# Convert ptype to lowercase

	# Set left/right limits for p(t)
	if (ptype=='rect'):
		kL = -0.5; kR = -kL
	else:
		kL = -1.0; kR = -kL		# Default left/right limits

	ixpL = int(ceil(Fs*kL/float(FB)))		# Left index for p(t) time axis
	ixpR = int(ceil(Fs*kR/float(FB)))		# Right index for p(t) time axis
	ttp = arange(ixpL,ixpR)/float(Fs) 	# Time axis for p(t)
	pt = zeros(len(ttp))				# Initialize pulse p(t)

	if (ptype=='rect'):		# Rectangular p(t)
		ix = where(logical_and(ttp>=kL/float(FB), ttp<kR/float(FB)))[0]
		pt[ix] = ones(len(ix))
	elif ptype == 'sinc': 	# Do the same thing for now..
		ix = where(logical_and(ttp>=kL/float(FB), ttp<kR/float(FB)))[0]
		pt[ix] = ones(len(ix))
	elif ptype == 'tri':	# Do nothing because don't need to
		pass
	else:
		print("ptype '%s' is not recognized" % ptype)

	# ***** Filter with h(t) = p(t) *****
	st = convolve(ast,pt)/float(Fs) 	# s(t) = a_s(t)*p(t)
	st = st[-ixpL:ixR-ixL-ixpL] 		# Trim after convolution

	# ***** Display as polar binary *****
	if polarity == 'polar':
		zero_val_indices = st < .5
		st[zero_val_indices] = -1
		zero_val_indices = an < .5
		an[zero_val_indices] = -1
		polarity = 'Polar'

	# ***** Sinc wave stuff *****
	m = 4	# Upsampling multiplier factor
	if ptype == 'sinc':
		if m == 1:		# If no upsampling, new signal s(t) just = an
			stm = an
		else:			# If upsampling, add zeros
			stm = an
			for i in range(0,(m-1)):
				stm = vstack([stm, zeros(len(an))])   # We need M-1 rows of zeros
			stm = stm.flatten('F')

		Fsm = m*Fs		# Increase the sampling frequency by the Upsampling
						# multiplier
		numTpBit = 1/(FB)*1/m 	# Get the number of samples
		tm = arange(0, (N*m)*numTpBit, numTpBit)	# Generate upsampled time axis

		k = pparms[0] 			# Sinc pulse truncation
		beta = pparms[1]		# Kaiser window factor
		fL = Fsm/8								# Cutoff frequency
		[tht, pt] = sinc_ipol.sinc(Fsm, fL, k)	# Generate sinc pulse
		pwt = pt*kaiser(len(pt), beta) 			# Pulse p(t), Kaiser windowed
		stfinal = convolve(stm, pwt, 'same')

	# ***** Plot *****
	figure(figsize = [10,4])
	numTpBit = int(len(st)/N)
	print("Samples/Bit: ", numTpBit)
	if ptype == 'rect':
		tvec_2a = arange(len(st))/float(Fs)
		plot(tvec_2a, st), grid()
		tvec_2a_2 = arange(numTpBit/2, N*numTpBit, numTpBit)/float(Fs)
		plot(tvec_2a_2, an, 'or')
		wave = 'Rectangular'
	elif ptype == 'tri':
		tvec_2a_2 = arange(numTpBit/2, N*numTpBit, numTpBit)/float(Fs)
		plot(tvec_2a_2, an, tvec_2a_2, an, 'or'), grid()
		wave = 'Triangular'
	elif ptype == 'sinc':
		plot(tm, stfinal/float(Fsm/m), tan, an, 'or'), grid()
		wave = 'Sinc'

	xlabel('t[sec]')
	ylabel('s(t), s(nT_B)')
	if ptype == 'tri' or ptype == 'rect':
		title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t)'.format(polarity, FB, wave))
	elif ptype == 'sinc':
		title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/beta$={:d}'.format(polarity, FB, wave, k, beta))

	show()
	return ecen.sigWave(st, Fs, t0) 	# Return waveform from sigWave class

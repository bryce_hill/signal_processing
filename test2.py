#!/usr/bin/env Python
from pylab import *
import numpy as np

# time = .03
# tau = arange(.001,time, .001)
# s = sin(pi*time/tau)

ptype = 'sinc'
k = 3
sps = 5

if ptype == 'rect':
	bound = np.floor(sps/float(2))
	pt = np.arange(-bound, bound, 1)
	pt[:] = 1
elif ptype == 'tri':
	bound = sps
	pt = np.arange(-bound, bound-1, 1, dtype = float)
	j = 1
	for i in range(0, bound):
		pt[i] = j/float(sps)
		j += 1
	j = sps
	for i in range(bound-1, 2*bound-1):
		pt[i] = j/float(sps)
		j -= 1
	t = arange(0,len(pt))
elif ptype == 'sinc':
	# Time axis
	ixk = int(round(Fs*k/float(2*fL)))
	tth = arange(-ixk,ixk+1)/float(Fs)

	# Sinc pulse
	ht = 2.0*fL*ones(len(tth))
	ixh = where(tth != 0.0)[0]
	ht[ixh] = sin(2*pi*fL*tth[ixh])/(pi*tth[ixh])
else:
	pass

print(pt)

#!/bin/bash

#
echo "Installing Bryce Hill's python packages in develop mode"
echo "Packages can be uninstalled by running the: "
echo "uninstall_bryce_lib.sh script"

cd my_library/my_grc
sudo python setup.py develop
sudo python3 setup.py develop

cd ../my_ascbin
sudo python setup.py develop
sudo python3 setup.py develop

cd ../my_ft
sudo python setup.py develop
sudo python3 setup.py develop

cd ../my_sinecos
sudo python setup.py develop
sudo python3 setup.py develop

cd ../my_sdr
sudo python setup.py develop
sudo python3 setup.py develop

cd ../my_jupyter
sudo python setup.py develop
sudo python3 setup.py develop

cd ../my_am
sudo python setup.py develop
sudo python3 setup.py develop

cd ../my_shiftKeying
sudo python setup.py develop
sudo python3 setup.py develop

#!/bin/bash

echo "Copying communications library functions to python site-packages path for use in GNU Radio"

yes | sudo cp -a my_library/gnuradiopkg/gnuradiopkg/ptfun.py /usr/local/lib/python2.7/dist-packages

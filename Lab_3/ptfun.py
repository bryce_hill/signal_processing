# this module will be imported in the into your flowgraph

# File: ptfun
# Functions for gnuradio-companion PAM p(t) generation
from pylab import *
import numpy as np
def pampt(sps, ptype, Fs, pparms=[]):
	"""
	PAM pulse p(t) = p(n*TB/sps) generation
	>>>>> pt = pampt(sps, ptype, pparms) <<<<<
	where 	sps:
			ptype: 	pulse type ('rect', 'sinc', 'tri')
			pparms not used for 'rect', 'tri'
			pparms = [k, beta] for sinc
			k:		"tail" truncation parameter for 'sinc'
					(truncates p(t) to -k*sps <= n < k*sps)
			beta: 	Kaiser window parameter for 'sinc'
			pt:		pulse p(t) at t=n*TB/sps
			Note: 	In terms of sampling rate Fs and baud rate FB,
					sps = Fs/FB
	"""
	fL = Fs/float(6)
	k = pparms[0]
	if ptype == 'rect':
		bound = np.floor(sps/float(2))
		pt = np.arange(-bound, bound, 1)
		pt[:] = 1
	elif ptype == 'tri':
		bound = sps
		pt = np.arange(-bound, bound-1, 1, dtype = float)
		j = 1
		for i in range(0, bound):
			pt[i] = j/float(sps)
			j += 1
		j = sps
		for i in range(bound-1, 2*bound-1):
			pt[i] = j/float(sps)
			j -= 1
	elif ptype == 'sinc':
		ixk = int(round(Fs*k/float(2*fL)))
		tth = np.arange(-ixk, ixk+1)/float(Fs)
		
		ht = 2.0*fL*np.ones(len(tth))
		ixh = np.where(tth != 0.0)[0]
		ht[ixh] = sin(2*pi*fL*tth[ixh])/(pi*tth[ixh])
		pt = ht/(Fs/3)
	else:
		pass
		
	return pt

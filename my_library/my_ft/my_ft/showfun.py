# File: showfun.py
# "show" functions like showft, showspd, etc
from pylab import *
import numpy as np

def showft(sig_xt, ff_lim, feature):
	"""
	Plot (DFT/FFT approximation to) Fourier transform of
	waveform x(t). Displays magnitude |X(f)| either linear
	and absolute or normalized (wrt to maximum value) in dB.
	Phase of X(f) is shown in degrees.
	>>>>> showft(sig_xt, ff_lim) <<<<<
	where	sig_xt:				waveform from class sigWave
			sig_xt.timeAxis():	time axis for x(t)
			sig_xt.signal():	sampled CT signal x(t)
			ff_lim = 	[f1, f2, llim]
			f1:			lower frequency limit for display
			f2:			upper frequency limit for display
			llim = 0:	dispay |X(f)| linear and absolute
			llim > 0:	same as llim =0 but phase is maksed
						(set to zero) for |X(f)|< llim
			llim < 0:	display 20*log_{10}(|X(f)|/max(|X(f)|)
						in dB with lower display limit llim dB,
						phase is masked (set to zero) for f
						with magnitude (dB, normalized) < llim
			feature:	= 0 | 1 | 2 depending on the amound of
						features wanted. To complete this lab
						I had to implement something like this
						or have 3 different showft functions.
						I have to always run the whole ipython
						script from scratch when I change external
						files so the method described in the description
						using the "reload" feature doesn't work.
	"""
	# ***** Preamble/housekeeping *****
	# Define f1, f2, and llim for ease of readability
	f1 = float(ff_lim[0])
	f2 = float(ff_lim[1])
	llim = float(ff_lim[2])
	# ***** Prepare x(t), swap pos/neg parts of time axis *****
	N = len(sig_xt)					# Blocklength of DFT/FFT
	Fs = sig_xt.get_Fs()			# Sampling rate
	tt = sig_xt.timeAxis()			# Get time axis for x(t)
	ixp = where(tt>=0)[0]			# Indices for t >= 0
	ixn = where(tt<0)[0]			# Indices for t < 0
	xt = sig_xt.signal()			# Get x(t)
	xt = hstack((xt[ixp], xt[ixn]))	# Swap pos/neg time axis

	# ***** Compute X(f), make frequency axis *****
	Xf = fft(xt)/float(Fs)			# DFT/FFT of x(t)
									# Scaled for X(f) approximation
	ff = Fs*array(arange(N),int64)/float(N) 	# Frequency axis

	# ***** Compute |X(f)|, arg[X(f)] *****
		# ****
	print("f1: ", f1, "| f2: ", f2, " | llim: ", llim)
	if feature == 0:
		absXf = abs(Xf)					# Magnitude |X(f)|
		argXf = angle(Xf)				# Phase arg[X(f)]
	if feature > 0:
		if ff_lim[0] < 0:
			ixn_seg2 = ff >= int(Fs/2)
			ixp_seg1 = ff < int(Fs/2)
			ff = np.concatenate((ff[ixn_seg2]-Fs, ff[ixp_seg1]))
			Xf = np.concatenate((Xf[ixn_seg2], Xf[ixp_seg1]))
		absXf = abs(Xf)					# Magnitude |X(f)|
		argXf = angle(Xf)				# Phase arg[X(f)]

	if feature == 2:
		absXf = 20*log10(abs(Xf)/max(abs(Xf)))	# Magnitude |X(f)|
		argXf = angle(Xf)						# Phase arg[X(f)]

		# **** Process X(f) for showing (1.(a).ii) ****
	if feature == 1:
		if float(llim) > float(0): # Mask to zero any X(f) < llim
			low_value_indices = absXf < llim
			argXf[low_value_indices] = 0
	elif feature == 2:
		# if float(pow(10,llim/20)) > float(0): # Mask to zero any X(f) < llim
		if float(llim) < float(0):
			low_value_indices = absXf < llim
			argXf[low_value_indices] = 0

	# ***** Plot magnitude/phase *****
	f1 = figure()
	af11 = f1.add_subplot(211)
	af11.plot(ff,absXf)			# Plot magnitude
	af11.grid()
		# **** Set Axis Labels ****
	if feature < 2:
		af11.set_ylabel('|X(f)|')
	elif feature == 2:
		af11.set_ylabel('|X(f)| [dB]')
	strgt = 'FT Approximation, $F_s=$' + str(Fs) + ' Hz'
	strgt = strgt + ', N=' + str(N)
	strgt = strgt + ', $\Delta_f$={0:3.2f}'.format(Fs/float(N)) + ' Hz'
	af11.set_title(strgt, size = 30)
	af12 = f1.add_subplot(212)
	af12.plot(ff,180/pi*argXf) 	# Plot phase in degrees
	af12.grid()
	af12.set_ylabel('arg[X(f)] [deg]')
	af12.set_xlabel('f [Hz]')

		# **** Set Axis Limits ****
	if feature > 0:
		af11.set_xlim(ff_lim[0], ff_lim[1])
		af12.set_xlim(ff_lim[0], ff_lim[1])

	if feature == 2:
		af11.set_ylim(llim, 0)

	show()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def showeye(sig_rt, FB, NTd=50, dispparms=[]):
	"""
		Display eye diagram of digital PAM signal r(t)
		>>>>> showeye(sig_rt, FB, NTd, dispparms) <<<<<
		where 		sig_rt: 	waveform from class sigWave
			sig_rt.signal(): 	received PAM signal
								r(t)=sum_n a_n*q(t-nTB)
			sig_rt.get_Fs(): 	sampling rate for r(t)
			FB: 				Baud rate of DT sequence a_n, TB = 1/FB
			NTd:			 	Number of traces displayed
			dispparms = [delay, width, ylim1, ylim2]
			delay: 				trigger delay (in TB units, e.g., 0.5)
			width:	 			display width (in TB units, e.g., 3)
			ylim1: 				lower display limit, vertical axis
			ylim2: 				upper display limit, vertical axis
	"""

	rt = sig_rt.signal()			# Get r(t)
	Fs = sig_rt.get_Fs()			# Sampling rate
	t0 = dispparms[0]/float(FB) 	# Delay in sec
	tw = dispparms[1]/float(FB) 	# Display width in sec
	dws = floor(Fs*tw)				# Display width in samples
	tteye = arange(dws)/float(Fs) 	# Time axis for eye
	trix = around(Fs*(t0+arange(NTd)/float(FB)))
	ix = where(logical_and(trix>=0, trix<=len(rt)-dws))[0]
	trix = trix[ix]								# Trigger indexes within r(t)
	TM = rt[int(trix[0]):int(trix[0]+dws)]				# First trace
	for i in range(1,NTd):
		TM = vstack((TM, rt[int(trix[i]):int(trix[i]+dws)]))	# All subsequent traces

	figure()
	plot(FB*tteye, TM.T, '-b') 					# Plot transpose of TM
	title('Eye Diagram for r(t) with $F_B$= {:d} Baud, $t_0$ = {:f}*$T_B$, # Traces = {:d}'.format(int(FB), t0, NTd))
	xlabel("$t/T_B$")
	ylabel("r(t)")
	grid()
	show()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def showpsd0(sig_xt, ff_lim, N):
	"""
	Plot (DFT/FFT approximation to) power spectral density (PSD) of x(t).
	Displays S_x(f) either linear and absolute or normalized in dB.
	>>>>> showpsd(sig_xt, ff_lim, N) <<<<<
	where 	sig_xt: 			waveform from class sigWave
			sig_xt.signal(): 	sampled CT signal x(t)
			sig_xt.get_Fs(): 	sampling rate of x(t)
			ff_lim = [f1,f2,llim]
			f1:					lower frequency limit for display
			f2:					upper frequency limit for display
			llim = 0: 			display S_x(f) linear and absolute
			llim < 0:	 		display 10*log_{10}(S_x(f))/max(S_x(f))
								in dB with lower display limit llim dB
			N:					blocklength (usually Fs)
	"""
	# ***** Determine number of blocks, prepare x(t) *****
	xt = sig_xt.signal()		# Get x(t)
	Fs = sig_xt.get_Fs()		# Sampling rate of x(t)
	N = int(min(N, len(xt)))	# N <= length(xt) needed
	NN = int(floor(len(xt)/float(N)))	# Number of blocks of length N
	xt = xt[0:N*NN]				# Truncate x(t) to NN blocks
	xNN = reshape(xt,(NN,N))	# NN row vectors of length N

	# ***** Compute DFTs/FFTs, average over NN blocks *****
	Sxf = np.power(abs(fft(xNN)),2.0) 	# NN FFTs, mag squared
	if NN > 1:
		Sxf = sum(Sxf, axis=0)/float(NN)
	Sxf = Sxf/float(N*Fs)		# Correction factor DFT -> PSD
	Sxf = reshape(Sxf,size(Sxf))
	ff = Fs*array(arange(N),int64)/float(N) # Frequency axis
	if ff_lim[0] < 0:						# Negative f1 case
		ixp = where(ff<0.5*Fs)[0]			# Indexes of pos frequencies
		ixn = where(ff>=0.5*Fs)[0] 			# Indexes of neg frequencies
		ff = hstack((ff[ixn]-Fs,ff[ixp])) 	# New freq axis
		Sxf = hstack((Sxf[ixn],Sxf[ixp])) 	# Corresponding S_x(f)
	df = Fs/float(N)			# Delta_f, freq sample spacing

	# ***** Calculate Total Power *****
	dt = N/float(Fs)		# Calculate time interval
	px = sum(Sxf)/float(dt)	# Calculate power
	px = round(px, 2)

	# ***** Determine maximum, trim to ff_lim *****
	maxSxf = max(Sxf)			# Maximum of S_x(f)
	ixf = where(logical_and(ff>=ff_lim[0], ff<ff_lim[1]))[0]
	ff = ff[ixf]				# Trim to ff_lim specs
	Sxf = Sxf[ixf]
	disp = 'Linear/Absolute'
	strgy = '$S_x(f)$'

	# ***** Power Calculation *****
	# dt = len(ixf)/float(Fs)		# Calculate time interval
	pxw = sum(Sxf)*df	# Calculate windowed power
	pxw = round(pxw, 3)

	# ***** Plot in dB scale if specified *****
	if ff_lim[2] < 0:
		ll_lim = ff_lim[2]
		Sxf = 10*log10(Sxf/max(Sxf))
		disp = 'dB'
		strgy = '10$log_{10}$($S_x(f)$) [dB]'


	# ***** Plot PSD *****
	strgt = 'PSD [{:s}] Approximation, $P_x$ = {:f}, $P_x(f_1,f_2)$={:f}, $F_s=${:d} Hz'.format(disp, px, pxw, Fs)
	strgt = strgt + ', $\\Delta_f=${:.3g} Hz'.format(df)
	strgt = strgt + ', $NN=${:d}, $N=${:d}'.format(NN, N)
	# f1 = figure()
	f1 = figure()
	af1 = f1.add_subplot(111)
	if ff_lim[2] < 0:
		af1.set_ylim(ff_lim[2], 0)
	af1.set_xlim(ff_lim[0], ff_lim[1])
	af1.plot(ff, Sxf, '-b')
	af1.grid()
	af1.set_xlabel('f [Hz]')
	af1.set_ylabel(strgy)
	af1.set_title(strgt)
	show()

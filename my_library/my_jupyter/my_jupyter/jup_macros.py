# File: Macros.py

# TODO: For some reason, some parameters don't work if it is a subplot

from pylab import *

def my_settings():
	'''
	Run some setup stuff for this ipython notebook, plot formatting, ..., etc.
	'''
	# ***** Set sizes for axes *****
		# Code taken from: http://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot
	print("Plot settings imported into current workspace")
	SMALL_SIZE = 14
	MEDIUM_SIZE = 20
	BIGGER_SIZE = 32
	matplotlib.rc('font', size=BIGGER_SIZE)          # controls default text sizes
	matplotlib.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
	matplotlib.rc('axes', labelsize=MEDIUM_SIZE)     # fontsize of the x and y labels
	matplotlib.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
	matplotlib.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
	matplotlib.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
	matplotlib.rc('figure', titlesize=BIGGER_SIZE)   # fontsize of the figure title

	# Set size for all figures
	# rcParams['figure.figsize'] = 8, 3    # Good size for %matplotlib notebook
	rcParams['figure.figsize'] = 15, 4.5    # Good size for %matplotlib inline

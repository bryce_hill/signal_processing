from setuptools import setup

setup(name="my_jupyter",
      version = "0.0",
      description = "Custom python modules for use with Jupyter Notebook",
      author = "Bryce Hill",
      packages=["my_jupyter"],
)


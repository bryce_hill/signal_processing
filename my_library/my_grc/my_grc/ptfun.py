# File: ptfun
# Functions for gnuradio-companion PAM p(t) generation
from pylab import *
import numpy as np
def testmy_grc():
    return('Package gnuradiopkg imported correctly')

def pampt(sps, ptype, pparms=[]):
	"""
	PAM pulse p(t) = p(n*TB/sps) generation
	>>>>> pt = pampt(sps, ptype, pparms) <<<<<
	where 	sps: 	samples/second = discrete time version of FS/FB
			ptype:	pulse type from list
					('man', 'rcf', 'rect', 'rrcf', 'sinc', 'tri')
			pparms: not used for 'man', 'rect', 'tri'
					pparms = [k, alpha] for 'rcf', 'rrcf'
					pparms = [k, beta] for 'sinc'
			k:		"tail" truncation parameter for 'rcf', 'rrcf', 'sinc'
					(truncates p(t) to -k*sps <= n < k*sps)
			alpha: 	Rolloff parameter for 'rcf', 'rrcf', 0 <= alpha <= 1
			beta: 	Kaiser window parameter for 'sinc'
			pt:		pulse p(t) at t=n*TB/sps
			Note: 	In terms of sampling rate Fs and baud rate FB,
					sps = Fs/FB
	"""

	k = pparms[0]
	if ptype == 'rect':
		bound = np.floor(sps/float(2))
		pt = np.arange(-bound, bound, 1)
		pt[:] = 1
	elif ptype == 'tri':
		bound = sps
		pt = np.arange(-bound, bound-1, 1, dtype = float)
		j = 1
		for i in range(0, bound):
			pt[i] = j/float(sps)
			j += 1
		j = sps
		for i in range(bound-1, 2*bound-1):
			pt[i] = j/float(sps)
			j -= 1
	elif ptype == 'man':
		bound = np.floor(sps/float(2))
		pt = np.arange(-bound, bound, 1)
		ixn = where(pt<0)[0]
		ixp = where(pt>=0)[0]
		pt[ixn] = -1
		pt[ixp] = 1
	elif ptype == 'sinc':
		pt = np.sinc(np.arange(-k*sps,k*sps)/float(sps))
		pt = pt*kaiser(len(pt), pparms[1])
	elif ptype == 'rcf':
		alpha = pparms[1]
		t = arange(-k*sps, k*sps)
		pt = np.sinc(t)
		pt = pt*cos(alpha*t)/(1-(2*alpha*t)**2)
	elif ptype == 'rrcf':
		alpha = pparms[1]
		t = arange(-k*sps, k*sps)
		pt = zeros(len(t))
		trange = sps/(4.0*alpha)
		# Get indices where t = +/- TB/(4*alpha)
		ix_tr = where(abs(t) == trange)
		# Get middle band between -trange< ixmid < trange and eliminate the
		#	index where t = 0
		ixmid = where(logical_and(logical_and(t>-trange, t<trange),t !=0))[0]
		# Get index where t = 0
		ix0 = where(t == 0)[0]
		# Get outside band s.t. ixout < -trange & ixout > trange
		ixout = where(logical_or(t>=trange, t<-trange))[0]
		# Filter out ix_tr values from the ixout indices
		ixout = [x for x in ixout if x not in ix_tr]

		ixmid = [x for x in ixmid if x not in ix0]

		pt[ix0] = 1.0-alpha+(4.0*alpha)/pi
		pt[ixmid] = sps/pi*(sin((1-alpha)*pi*t[ixmid]/sps)+(4.0*alpha*t[ixmid]/sps)*cos((1+alpha)*pi*t[ixmid]/sps))/((1.0-(4.0*alpha*t[ixmid]/sps)**2.0)*t[ixmid])
		# TODO Need to get rid of ix_tr indices from the ixout ones to avoid runtime errors
		pt[ixout] = sps/pi*(sin((1-alpha)*pi*t[ixout]/sps)+(4.0*alpha*t[ixout]/sps)*cos((1+alpha)*pi*t[ixout]/sps))/((1.0-(4.0*alpha*t[ixout]/sps)**2.0)*t[ixout])
		pt[ix_tr] = alpha/float(2**(.5))*((1+2/pi)*sin(pi/(4.0*alpha))+(1-2/pi)*cos(pi/(4*alpha)))
	else:
		pass

	return pt
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def pamhRt(sps, ptype, pparms=[]):
	"""
	PAM normalized matched filter (MF) receiver filter
	h_R(t) = h_R(n*TB/sps) generation
	>>>>> hRt = pamhRt(sps, ptype, pparms) <<<<<
	where 	sps:	samples/second = discrete time version of FS/FB
			ptype: 	pulse type from list
					('man', 'rcf', 'rect', 'rrcf', 'sinc', 'tri')
			pparms: not used for 'man', 'rect', 'tri'
					pparms = [k, alpha] for 'rcf', 'rrcf'
					pparms = [k, beta] for 'sinc'
			k:		"tail" truncation parameter for 'rcf', 'rrcf', 'sinc'
					(truncates p(t) to -k*sps <= n < k*sps)
			alpha: 	Rolloff parameter for 'rcf', 'rrcf', 0 <= alpha <= 1
			beta: 	Kaiser window parameter for 'sinc'
			hRt:	MF impulse response h_R(t) at t=n*TB/sps
			Note: 	In terms of sampling rate Fs and baud rate FB,
					sps = Fs/FB
	"""
	# Use pampt to get the pulse
	pt = pampt(sps, ptype, pparms)
	# reverse the pulse (doesn't affect most of them)
	ht = pt[::-1]
	# normalize by sum (squared) (From definition of a matched filter)
	hRt = ht/sum(np.power(ht,2))
	return hRt

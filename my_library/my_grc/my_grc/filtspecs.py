# File: filtspecs.py
# Module for filter specfifications
# FIR: 	Determine filter taps
# IIR:  Determine numerator (b) and denominator (a)
# 		polynomial coefficients
from pylab import *
import my_sdr as sdr
# Are we using butter??????????
from scipy.signal import butter

def trapfilt_taps(N, phiL, alpha, splot):
	"""
	Returns taps for order N FIR LPF with trapezoidal frequency
	response, normalized cutoff frequency phiL = fL/Fs, and rolloff
	parameter alpha.
	>>>>> hLn = trapfilt_taps(N, phiL, alpha) <<<<<
	where 	N:		filter order (k*sps)
			phiL: 	normalized cutoff frequency (-6 dB)
			alpha: 	frequency rolloff parameter, linear rolloff
					over range (1-alpha)phiL <= |f| <= (1+alpha)phiL
			splot:	if splot > 1 enable plotting for debugging purposes
	"""
	# What am I generating here? ht from trapfilt0?

	# ixk = round(N/float(2*phiL)) 	# Tail cutoff index
	ixk = round((N)/2)
	print("Tail Cuttoff Index: ", ixk)
	tth = arange(-ixk,ixk+1)#/float(100/(phiL))
	print('len(tth) :', len(tth))
	# print('dt: ', phiL/100)

	# ***** Generate impulse response ht here *****
	ht = zeros(len(tth))		# Generate ht of correct size
	# Logical and out the index where tth = 0 as this will break the function
	ix0 = where(tth == 0)[0]
	ix = where(logical_and(logical_and(tth>tth[0], tth<tth[len(tth)-1]),tth !=0))[0]
	ht[ix] = sin(2.0*pi*phiL*tth[ix])/(1.0*pi*tth[ix])
	ht[ix] = ht[ix]*sin(2.0*pi*alpha*phiL*tth[ix])/(2.0*pi*alpha*phiL*tth[ix])
	ht[ix0] = phiL*2				# Set value for tth = 0 case

	# ***** If plotting requested, plot ht *****
	if splot > 0:
		figure()
		plot(tth, ht), grid()
		title('Pulse h(t)')
		xlabel('Time [TB*Fs]')
		show()

	return ht				# Return?

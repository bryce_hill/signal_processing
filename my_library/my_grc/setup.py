from setuptools import setup

setup(name="my_grc",
      version = "0.0",
      description = "Custom python modules for gnuradio",
      author = "Bryce Hill",
      packages=["my_grc"],
)


from setuptools import setup

setup(name="my_am",
      version = "0.0",
      description = "Custom python modules sofware defined AM radio",
      author = "Bryce Hill",
      packages=["my_am"],
)

# File: amfun.py
# Module for AM radio applications
from pylab import *
# from scipy.signal import butter, lfilter
import my_sdr as sdr
import numpy as np


def amxmtr(sig_mt, xtype, fcparms, fmparms=[], fBparms=[]):
	"""
	Amplitude Modulation Transmitter for suppressed ('sc')
	and transmitted ('tc') carrier AM signals
	>>>>> sig_xt = amxmtr(sig_mt, xtype, fcparms, fmparms, fBparms) <<<<<
	where
	sig_xt: 		waveform from class sigWave
	sig_xt.signal():	transmitted AM signal
	sig_xt.timeAxis(): 	time axis for x(t)
	sig_mt: 			waveform from class sigWave
	sig_mt.signal():	modulating (wideband) message signal
	sig_mt.timeAxis(): 	time axis for m(t)
	xtype: 				'sc' or 'tc' (suppressed or transmitted carrier)
	fcparms:			= [fc, thetac] for 'sc'
	fcparms: 			= [fc, thetac, alpha] for 'tc'
	fc:					carrier frequency
	thetac: 			carrier phase in deg (0: cos, -90: sin)
	alpha:				modulation index 0 <= alpha <= 1
	A:					carrier amplitude
	fmparms: 			= [fm, km, alpham] LPF at fm parameters
						no LPF at fm if fmparms = []
	fm:					highest message frequency
	km:					LPF h(t) truncation to |t| <= km/(2*fm)
	alpham: 				LPF at fm frequency rolloff parameter, linear
						rolloff over range 2*alpham*fm
	fBparms: 			= [fBW, fcB, kB, alphaB] BPF at fcB parameters
						no BPF if fBparms = []
	fBW:				-6 dB BW of BPF
	fcB:				center freq of BPF
	kB:					BPF h(t) truncation to |t| <= kB/fBW
	alphaB: 				BPF frequency rolloff parameter, linear
						rolloff over range alphaB*fBW
	"""
	fc = fcparms[0]
	thetac = fcparms[1]
	alpha = fcparms[2]

	if fmparms != []:
		fm = fmparms[0]
		km = fmparms[1]
		alpham = fmparms[2]

	if fBparms != []:
		fBW = fBparms[0]
		fcB = fBparms[1]
		kB = fBparms[2]

	# ***** Step 0: Filter the Signal *****
	if fmparms != 0:
		[sig_mt_f, order]= sdr.trapfilt(sig_mt, [fm, 0], km, alpham, 0, 0)
	else:
		print('Warning: no low pass frequency provided and message not LPFd')

	mt = sig_mt_f.signal()	# Input signal
	Fs = sig_mt_f.get_Fs()	# Sampling rate
	tmt = sig_mt.timeAxis() # Get time axis

	# ***** Step 1: Carrier *****
	if xtype == 'sc':		# Supressed carrier
		xt = mt*cos(2*pi*fc*tmt + thetac)
	elif xtype == 'tc':	# Transmitted carrier
		xt = (1 + alpham*mt)*cos(2*pi*fc*tmt + thetac)
	else:
		print('Warning: Invalid carrier type specified')

	xt = sdr.sigWave(xt, Fs, sig_mt_f.get_t0())

	# ***** Step 2: Bandbass? *****
	if fBparms != []:
		[xt, order] = sdr.trapfilt(xt, [fBW, fcB], kB, alphaB, 0, 0)	# Bandpass filter

	# ***** Step 3: Return *****
	return xt

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def amrcvr(sig_rt, rtype, fcparms, fmparms=[], fBparms=[], dcblock=False):
	"""
	Amplitude Modulation Receiver for coherent ('coh') reception,
	or absolute value ('abs'), or squaring ('sqr') demodulation,
	or I-Q envelope ('iqabs') detection, or I-Q phase ('iqangle')
	detection.
	>>>>> sig_mthat = amrcvr(sig_rt, rtype, fcparms, fmparms, fBparms, dcblock) <<<<<
	where
	sig_mthat: 				waveform from class sigWave
	sig_mthat.signal():		demodulated message signal
	sig_mthat.timeAxis(): 	time axis mhat(t)
	sig_rt: 				waveform from class sigWave
	sig_rt.signal():		received AM signal
	sig_rt.timeAxis(): 		time axis for r(t)
	rtype: 					Receiver type from list
							'abs' (absolute value envelope detector),
							'coh' (coherent),
							'iqangle' (I-Q rcvr, angle or phase),
							'iqabs' (I-Q rcvr, absolute value or envelope),
							'sqr' (squaring envelope detector)
	fcparms: 				= [fc, thetac]
	fc:						carrier frequency
	thetac: 				carrier phase in deg (0: cos, -90: sin)
	fmparms: 				= [fm, km, alfam] LPF at fm parameters
							no LPF at fm if fmparms = []
	fm:						highest message frequency
	km:						LPF h(t) truncation to |t| <= km/(2*fm)
	alfam: 					LPF at fm frequency rolloff parameter, linear
							rolloff over range 2*alfam*fm
	fBparms: 				= [fBW, fcB, kB, alfaB] BPF at fcB parameters
							no BPF if fBparms = []
	fBW:					-6 dB BW of BPF
	fcB:					center freq of BPF
	kB:						BPF h(t) truncation to |t| <= kB/fBW
	alfaB: 					BPF frequency rolloff parameter, linear
							rolloff over range alfaB*fBW
	dcblock: 				remove dc component from mthat if true, compute
							the mean of the signal and subtract it from the
							signal
	"""
	# ***** Step 0: Get Signal Parameters *****
	fc = fcparms[0]
	thetac = fcparms[1]

	if fmparms != []:
		fm = fmparms[0]
		km = fmparms[1]
		alpham = fmparms[2]
	else:
		print('Warning: no fmparms specified, LPF will not work')

	if fBparms != []:
		fBW = fBparms[0]
		fcB = fBparms[1]
		kB = fBparms[2]
		alphaB = fBparms[3]

	# ***** Step 0: Bandbass? *****
	if fBparms != []:
		[sig_rt, order] = sdr.trapfilt(sig_rt, [fBW, fcB], kB, alphaB, 0, 0)	# Bandpass filter

	rt = sig_rt.signal()	# Input signal
	trt = sig_rt.timeAxis()	# Get time axis
	Fs = sig_rt.get_Fs()	# Sampling rate
	t0 = sig_rt.get_t0()    # Get starting time

	# ***** Step 1:  *****
	if rtype == 'coh':		# On pg. 2 of lab 8
		vt = rt*2*cos(2*pi*fc*trt + pi/180.0*thetac)		# Local oscillator
		vt = sdr.sigWave(vt, Fs, 0)
		[mhat, order] = sdr.trapfilt(vt, [fm, 0], km, alpham, 0, 0)	# Low pass filter
	elif rtype == 'abs':
		vt = sdr.sigWave(np.abs(rt), Fs, 0)
		[rhot, order] = sdr.trapfilt(vt, [fm, 0], km, alpham, 0, 0)	# Low Pass Filter
		if dcblock == True:											# Block DC
			rhot.sig = rhot.sig - np.mean(rhot.sig)
		mhat = rhot
	elif rtype == 'sqr':
		vt = sdr.sigWave(np.square(rt), Fs, 0)						# Square the input signal
		[wt, order] = sdr.trapfilt(vt, [fm, 0], km, alpham, 0, 0)	# Low Pass Filter
		rhot = np.sqrt(wt.signal())									# Square root of filtered signal
		if dcblock == True:											# Block DC
			rhot = rhot - np.mean(rhot)
		mhat = sdr.sigWave(rhot, Fs, 0)
	elif rtype == 'iqabs': # often does better job then sqr and abs rcvr for non-coherent signals, tend not to use sqr and abs for digital signlal processing because harmonics
		vi = rt*2*cos(2*pi*fc*trt)									# Separate into i and q parts
		vi = sdr.sigWave(vi, Fs, 0)
		vq = rt*-2*cos(2*pi*fc*trt)
		vq = sdr.sigWave(vq, Fs, 0)
		[wi, order] = sdr.trapfilt(vi, [fm, 0], km, alpham, 0, 0)	# Low Pass Filter
		[wq, order] = sdr.trapfilt(vq, [fm, 0], km, alpham, 0, 0)	# Low Pass Filter
		# Bring i and q parts back together
		rhohat = np.sqrt(np.square(wi.signal()) + np.square(wq.signal()))
		mhat = sdr.sigWave(rhohat, Fs, 0)
	elif rtype == 'iqangle':
		# Separate into i and q parts
		vi = rt*2*cos(2*pi*fc*trt)
		vi = sdr.sigWave(vi, Fs, 0)
		vq = rt*-2*cos(2*pi*fc*trt)
		vq = sdr.sigWave(vq, Fs, 0)
		# Low Pass Filter
		[wi, order] = sdr.trapfilt(vi, [fm, 0], km, alpham, 0, 0)
		[wq, order] = sdr.trapfilt(vq, [fm, 0], km, alpham, 0, 0)
		# Bring i and q parts back together
		thetahat = arctan(wi.sig/wq.sig)
		mhat = sdr.sigWave(thetahat, Fs, 0)


	# ***** Step : Return *****
	return mhat

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def qamxmtr(sig_mt, fcparms, fmparms=[]):
	"""
	Quadrature Amplitude Modulation (QAM) Transmitter with
	complex-valued input/output signals
	>>>>> sig_xt = qamxmtr(sig_mt, fcparms, fmparms) <<<<<
	where
	sig_xt: 			waveform from class sigWave
	sig_xt.signal():	complex-valued QAM signal
	sig_xt.timeAxis(): 	time axis for x(t)
	sig_mt.signal():	complex-valued (wideband) message signal
	sig_mt.timeAxis(): 	time axis for m(t)
	fcparms: 			= [fc, thetaci, thetacq]
	fc:					carrier frequency
	thetaci: 			in-phase (cos) carrier phase in deg
	thetacq: 			quadrature (sin) carrier phase in deg
	fmparms:	 		= [fm, km, alfam] for LPF at fm parameters
						no LPF/BPF at fm if fmparms = []
	fm:					highest message frequency (-6dB)
	km:					h(t) is truncated to |t| <= km/(2*fm)
	alfam:				frequency rolloff parameter, linear
						rolloff over range (1-alfam)*fm <= |f| <= (1+alfam)*fm
	"""

	# ***** Step 0: Get Signal Parameters *****
	fc = fcparms[0]
	thetaci = fcparms[1]
	thetacq = fcparms[2]

	if fmparms != []:
		fm = fmparms[0]
		km = fmparms[1]
		alpham = fmparms[2]
	else:
		print('Warning: no fmparms specified')

	mt = sig_mt.signal()	# Input signal
	tmt = sig_mt.timeAxis()	# Get time axis
	Fs = sig_mt.get_Fs()	# Sampling rate

	if fmparms != 0:
		[sig_mt_f, order]= sdr.trapfilt_cc(sig_mt, [fm, 0], km, alpham, 0, 0)
	else:
		print('Warning: no low pass frequency provided and message not LPFd')

	xt = mt*cos(2*pi*fc*tmt+thetaci) + mt*sin(2*pi*fc*tmt+thetacq)

	# ***** Step : Return *****
	return sdr.sigWave(xt, Fs, sig_mt.get_t0())

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def qamrcvr(sig_rt, fcparms, fmparms=[]):
	"""
	Quadrature Amplitude Modulation (QAM) Receiver with
	complex-valued input/output signals
	>>>>> sig_mthat = qamrcvr(sig_rt, fcparms, fmparms) <<<<<
	where
	sig_mthat: 				waveform from class sigWave
	sig_mthat.signal():		complex-valued demodulated message signal
	sig_mthat.timeAxis(): 	time axis for mhat(t)
	sig_rt: 				waveform from class sigWave
	sig_rt.signal():		received QAM signal (real- or complex-valued)
	sig_rt.timeAxis(): 		time axis for r(t)
	fcparms: 				= [fc, thetaci, thetacq]
	fc:						carrier frequency
	thetaci: 				in-phase (cos) carrier phase in deg
	thetacq: 				quadrature (sin) carrier phase in deg
	fmparms: 				= [fm, km, alfam]
	 						for LPF at fm parameters
							no LPF at fm if fmparms = []
	fm:						highest message frequency (-6 dB)
	km:						h(t) is truncated to |t| <= km/(2*fm)
	alfam:					frequency rolloff parameter, linear
							rolloff over range (1-alfam)*fm <= |f| <= (1+alfam)*fm
	"""
	# ***** Step 0: Get Signal Parameters *****
	fc = fcparms[0]
	thetaci = fcparms[1]
	thetacq = fcparms[2]

	if fmparms != []:
		fm = fmparms[0]
		km = fmparms[1]
		alpham = fmparms[2]
	else:
		print('Warning: no fmparms specified, LPF will not work')

	rt = sig_rt.signal()	# Input signal
	trt = sig_rt.timeAxis()	# Get time axis
	Fs = sig_rt.get_Fs()	# Sampling rate
	t0 = sig_rt.get_t0()    # Get starting time

	# ***** AM demodulation *****
	vit = rt*cos(2*pi*fc*trt+pi/180.0*thetaci)
	vqt = -rt*sin(2*pi*fc*trt+pi/180.0*thetacq)

	vit = sdr.sigWave(vit, Fs, t0)
	vqt = sdr.sigWave(vqt, Fs, t0)

	[mit, orderi] = sdr.trapfilt_cc(vit, [fm, 0], km, alpham, 0, 0)
	[mqt, orderq] = sdr.trapfilt_cc(vqt, [fm, 0], km, alpham, 0, 0)

	# TODO how do I form xt, what is the output?
	xL = mit.sig + 1j*mqt.sig

	# ***** Step : Return *****
	return sdr.sigWave(xL, Fs, sig_rt.get_t0())

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

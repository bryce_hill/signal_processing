from setuptools import setup

setup(name="my_sinecos",
      version = "0.0",
      description = "Custom python module for sine/cos applications",
      author = "Bryce Hill",
      packages=["my_sinecos"],
)

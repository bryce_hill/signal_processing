# File: sinc_ipol.py
# Sinc pulse for Interpolation
from pylab import *

# Inputs:
# 	Fs = sampling frequency,
# 	fL = lowpass filter cutoff frequency,
# 	k = sinc pulse truncation
def sinc(Fs, fL, k):
	# Time axis
	ixk = int(round(Fs*k/float(2*fL)))
	tth = arange(-ixk,ixk+1)/float(Fs)

	# Sinc pulse
	ht = 2.0*fL*ones(len(tth))
	ixh = where(tth != 0.0)[0]
	ht[ixh] = sin(2*pi*fL*tth[ixh])/(pi*tth[ixh])
	return tth,ht

	if __name__ == '__main__':
	  sinc()

# concerned with the Interpolation in the time domain. acts as low pass filter still but that isn't hte design intent
# Inputs:
# 	tth = time axis
#  	FB = Baud rate
# 	k = tail truncation parameter
# 	alpha = Rolloff parameter for 'rcf', 0<=alpha<=1
def rcf(tth, FB, k, alpha):
	ht = ones(len(tth))
	ixh = where(tth != 0.0)[0]
	ht[ixh] = sin(pi*FB*tth[ixh])/(FB*pi*tth[ixh])*cos(alpha*pi*FB*tth[ixh])/(1-(2*alpha*FB*tth[ixh])**2)
	return tth,ht

	if __name__ == '__main__':
	  rcf()

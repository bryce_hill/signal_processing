from .sinc_ipol import sinc
from .sinc_ipol import rcf
from .sine100 import sine100
from .sine100 import sineGen

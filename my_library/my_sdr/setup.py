from setuptools import setup

setup(name="my_sdr",
      version = "0.0",
      description = "Custom python modules sofware defined radio applications",
      author = "Bryce Hill",
      packages=["my_sdr"],
)

# File: pcmfun.py
# Functions for conversion between m(t) and PCM representation
from pylab import *
import numpy as np

	# He's generated a bitstream using PAM
	# We decode that to a bitstream as we were doing earlier
	# Then, we decode THAT with pcm2mt
	# Which should give us a signal...I think?
	# So:
	# wav --> ftpam_rcvr01 = dn --> pcm2mt = mt --> wav file

def threshold(numBits):
	"""

	"""
	# === Determine thresholds ===
	numQuant = int(pow(2, numBits)/2) # Number of quantization bits
	thresholds = zeros(2*numQuant-1)  # Define array to hold thresholds
	section = 1/numQuant              # Determine distance between thresholds
	index = 0                         # Define artifical index for the thresholds
	# Threshold partitions must be handled differently if # of quantizations even | odd
	if numQuant%2 == 0: # if even
	    print("even# of quantizations")
	    for i in arange(round(-len(thresholds)/2)+1, round(len(thresholds)/2)):
	#         print(i)
	        thresholds[index] = i*section
	        index += 1
	elif numQuant%2 == 1: # if odd
	    print("odd# of quantizations")
	    for i in arange(round(-len(thresholds)/2), round(len(thresholds)/2)+1):
	#         print(i)
	        thresholds[index] = i*section
	        index += 1
	# print(thresholds)
	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	return thresholds

def mt2pcm(mt, numBits):
	"""
	Message signal m(t) to binary PCM conversion
	>>>>> dn = mt2pcm(mt, bits) <<<<<
	where 	mt 		normalized (A=1) "analog" message signal
			bits	number of bits used per sample
			dn 		binary output sequence in sign-magnitude
					form, MSB (sign) first
	"""
	# === Determine thresholds ===
	thresholds = threshold(numBits)
	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	# === Generate array of decimal values. ===
	numBin = pow(2,numBits)  # Number of values
	x = np.arange(numBin-1, numBin/2-1, -1) # 1st array from minimum '-' value to 0
	y = np.arange(0, numBin/2)              # 2nd array from
	decimalRefTable = np.concatenate((x,y))
	# print(decimalRefTable)

	# Generate binary representation for each decimal value
	binaryTable = np.zeros((numBin, numBits))
	for i in range(0,numBin):
	    tmpBinary = binary_repr(int(decimalRefTable[i]), width=numBits)
	    for j in range(0,numBits):
	        binaryTable[i,j] = tmpBinary[j]

	# print(binaryTable)
	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	# === Read m(t) and assign binary output sequence ===
	dn = zeros(numBits*len(mt))
	for i in range(0, len(mt)):
	    dummy = 0
	    dummy2 = 0
	    index = 0
	#     print(mt[i])
	    while (dummy2 < 2*len(thresholds)):
	        if mt[i] < thresholds[0]: # Value is at lower extreme
	            index = 0
	            dummy2 = 2*len(thresholds)
	        elif thresholds[numBin-2] < mt[i]: # Value is at upper extreme
	            index = numBin -1
	            dummy2 = 2*len(thresholds)
	        elif mt[i] == 0:
	            index = numBin/2+1
	            dummy2 = 2*len(thresholds)
	        elif thresholds[dummy] < mt[i] < thresholds[dummy+1]:
	            index = dummy
	            dummy2 = 2*len(thresholds)
	        dummy += 1
	        dummy2 += 1
	#         print(dummy2)
	    for j in range(0, numBits):
	        dn[i*int(numBits) + j] = binaryTable[int(index), j]
	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	return dn

def pcm2mt(dn, numBits):
	"""
	Binary PCM to message signal m(t) conversion
	>>>>> mt = pcm2mt(dn, bits) <<<<<
	where 	dn 		binary output sequence in sign-magnitude
					form, MSB (sign) first
			bits	number of bits used per sample
			mt		normalized (A=1) "analog" message signal
	"""
	# === Determine thresholds ===
	thresholds = threshold(numBits)
	# print(thresholds)
	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	# === Get decimal value from binary ====
	numBinSeg = int(len(dn)/numBits) 	# Get number of binary segments
	binaryTable = np.zeros((int(numBinSeg), int(numBits)))	# Define table to partition binary segments into
	decimalTable = np.zeros(numBinSeg)
	for i in range(0, numBinSeg):
		# print("i =", i)
		k = numBits-1
		for j in range(0, numBits):
			# print("j = ", j)
			index = i*numBits + j
			# print("index = ", index)
			# print("k =", k)
			decimalTable[i] += round(dn[index])*pow(2, k)
			k -= 1
		# print(decimalTable[i])
	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	# === Convert discretized value (now in decimal code) to mt ===
	numBin = pow(2,numBits)  # Number of values
	x = np.arange(numBin-1, numBin/2-1, -1) # 1st array from minimum '-' value to 0
	y = np.arange(0, numBin/2)              # 2nd array from
	decimalRefTable = np.concatenate((x,y))
	# print(decimalRefTable)
	mt = np.zeros(numBinSeg)
	for i in range(0, numBinSeg):
		for j in range(0, len(decimalRefTable)):
			if decimalTable[i] == decimalRefTable[j]:
				index = j
				# print("Index", j)
		# Now that I know the index of the value, look it up in the threshold and assign to mt
		if index == 0:
			mt[i] = -1
			# print("mt", mt[i])
		elif index == len(decimalRefTable)-1:
			mt[i] = 1
			# print("mt", mt[i])
		else:
			mt[i] = thresholds[index] - (thresholds[index] - thresholds[index-1])/2
			# print("mt", mt[i])
	# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	return mt

# File: pamfun.py
# Functions for pulse amplitude modulation (PAM)
from pylab import *
import numpy as np
import my_sdr.ecen4652 as ecen
import my_ascbin.ascfun
from my_sinecos import sinc_ipol

def pam10(sig_an, Fs, ptype, polarity, pparms=[]):
	"""
	Pulse amplitude modulation: a_n -> s(t), -TB/2<=t<(N-1/2)*TB,
	V1.0 for 'rect', 'sinc', and 'tri' pulse types.
	>>>>> sig_st = pam10(sig_an, Fs, ptype, pparms) <<<<<
	where 	sig_an: 			sequence from class sigSequ
			sig_an.signal(): 	N-symbol DT input sequence a_n, 0 <= n < N
			sig_an.get_FB(): 	Baud rate of a_n, TB=1/FB
			Fs:					sampling rate of s(t)
			ptype: 				pulse type ('rect','sinc','tri')
			pparms:				pparms not used for 'rect','tri'
								pparms = [k, beta] for 'sinc'
			polarity:			display as unipolar or polar
			k:					"tail" truncation parameter for 'sinc'
								(truncates p(t) to -k*TB <= t < k*TB)
			beta: 				Kaiser window parameter for 'sinc'
			sig_st: 			waveform from class sigWave
			sig_st.timeAxis(): 	time axis for s(t), starts at -TB/2
			sig_st.signal():	CT output signal s(t), -TB/2<=t<(N-1/2)*TB,
								with sampling rate Fs
	"""
	# string = ascfun.bin2asc(sig_an.signal())

	N = len(sig_an)			# Number of data symbols
	FB = sig_an.get_FB()	# Baud rate
	n0 = sig_an.get_n0()	# Starting index
	ixL = int(ceil(-Fs*(n0+0.5)/float(FB)))	# Left index for time axis
	ixR = int(ceil(Fs*(n0+N-0.5)/float(FB))) # Right index for time axis
	tt = arange(ixL,ixR)/float(Fs) 		# Time axis for s(t)
	t0 = tt[0]				# Start time for s(t)

	# ***** Conversion from DT a_n to CT a_s(t) *****
	an = sig_an.signal()		# Sequence a_n
	tan = arange(0, len(an))/float(FB)
	# print("len(an): ", len(an), 'tan: ', tan)
	ast = zeros(len(tt))		# Initialize a_s(t)
	ix = array(around(Fs*arange(0,N)/float(FB)),int)	# Symbol center indexes
	ast[ix-int(ixL)] = Fs*an	# delta_n -> delta(t) conversion

	# ***** Set up PAM pulse p(t) *****
	ptype = ptype.lower()		# Convert ptype to lowercase

	# Set left/right limits for p(t)
	if (ptype=='rect'):
		kL = -0.5; kR = -kL
	else:
		kL = -1.0; kR = -kL		# Default left/right limits

	ixpL = int(ceil(Fs*kL/float(FB)))		# Left index for p(t) time axis
	ixpR = int(ceil(Fs*kR/float(FB)))		# Right index for p(t) time axis
	ttp = arange(ixpL,ixpR)/float(Fs) 	# Time axis for p(t)
	pt = zeros(len(ttp))				# Initialize pulse p(t)

	if (ptype=='rect'):		# Rectangular p(t)
		ix = where(logical_and(ttp>=kL/float(FB), ttp<kR/float(FB)))[0]
		pt[ix] = ones(len(ix))
	elif ptype == 'sinc': 	# Do the same thing for now..
		ix = where(logical_and(ttp>=kL/float(FB), ttp<kR/float(FB)))[0]
		pt[ix] = ones(len(ix))
	elif ptype == 'tri':	# Do nothing because don't need to
		pass
	else:
		print("ptype '%s' is not recognized" % ptype)

	# ***** Filter with h(t) = p(t) *****
	st = convolve(ast,pt)/float(Fs) 	# s(t) = a_s(t)*p(t)
	st = st[-ixpL:ixR-ixL-ixpL] 		# Trim after convolution

	# ***** Display as polar binary *****
	if polarity == 'polar':
		zero_val_indices = st < .5
		st[zero_val_indices] = -1
		zero_val_indices = an < .5
		an[zero_val_indices] = -1
		polarity = 'Polar'

	# ***** Sinc wave stuff *****
	m = 4	# Upsampling multiplier factor
	if ptype == 'sinc':
		if m == 1:		# If no upsampling, new signal s(t) just = an
			stm = an
		else:			# If upsampling, add zeros
			stm = an
			for i in range(0,(m-1)):
				stm = vstack([stm, zeros(len(an))])   # We need M-1 rows of zeros
			stm = stm.flatten('F')

		Fsm = m*Fs		# Increase the sampling frequency by the Upsampling
						# multiplier
		numTpBit = 1/(FB)*1/m 	# Get the number of samples
		tm = arange(0, (N*m)*numTpBit, numTpBit)	# Generate upsampled time axis

		k = pparms[0] 			# Sinc pulse truncation
		beta = pparms[1]		# Kaiser window factor
		fL = Fsm/8								# Cutoff frequency
		[tht, pt] = sinc_ipol.sinc(Fsm, fL, k)	# Generate sinc pulse
		pwt = pt*kaiser(len(pt), beta) 			# Pulse p(t), Kaiser windowed
		stfinal = convolve(stm, pwt, 'same')

	# ***** Plot *****
	figure()
	numTpBit = int(len(st)/N)
	print("Samples/Bit: ", numTpBit)
	if ptype == 'rect':
		tvec_2a = arange(len(st))/float(Fs)
		plot(tvec_2a, st), grid()
		tvec_2a_2 = arange(numTpBit/2, N*numTpBit, numTpBit)/float(Fs)
		plot(tvec_2a_2, an, 'or')
		wave = 'Rectangular'
	elif ptype == 'tri':
		tvec_2a_2 = arange(numTpBit/2, N*numTpBit, numTpBit)/float(Fs)
		plot(tvec_2a_2, an, tvec_2a_2, an, 'or'), grid()
		wave = 'Triangular'
	elif ptype == 'sinc':
		plot(tm, stfinal/float(Fsm/m), tan, an, 'or'), grid()
		wave = 'Sinc'

	xlabel('t[sec]')
	ylabel('s(t), s(nT_B)')
	if ptype == 'tri' or ptype == 'rect':
		title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t)'.format(polarity, FB, wave))
	elif ptype == 'sinc':
		title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/beta$={:d}'.format(polarity, FB, wave, k, beta))

	show()
	return ecen.sigWave(st, Fs, t0) 	# Return waveform from sigWave class

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def pam11(sig_an, Fs, ptype, polarity, splot, pparms=[]):
	"""
	Pulse amplitude modulation: a_n -> s(t), -TB/2<=t<(N-1/2)*TB,
	V1.1 for 'man', 'rcf', 'rect', 'sinc', and 'tri' pulse types.
	>>>>> sig_st = pam11(sig_an, Fs, ptype, pparms) <<<<<
	where 		sig_an: 			sequence from class sigSequ
				sig_an.signal(): 	N-symbol DT input sequence a_n, 0 <= n < N
				sig_an.get_FB(): 	Baud rate of a_n, TB=1/FB
				Fs:	 				sampling rate of s(t)
				ptype: 				pulse type from list ('man','rcf','rect','sinc','tri')
				pparms not used for 'man','rect','tri'
				pparms = [k, alpha] for 'rcf'
				pparms = [k, beta] for 'sinc'
				k:					"tail" truncation parameter for 'rcf','sinc'
									(truncates p(t) to -k*TB <= t < k*TB)
				alpha: 				Rolloff parameter for 'rcf', 0<=alpha<=1
				beta: 				Kaiser window parameter for 'sinc'
				sig_st: 			waveform from class sigWave
				sig_st.timeAxis(): 	time axis for s(t), starts at -TB/2
				sig_st.signal():	CT output signal s(t), -TB/2<=t<(N-1/2)*TB,
									with sampling rate Fs
	"""
	N = len(sig_an)			# Number of data symbols
	FB = sig_an.get_FB()	# Baud rate
	n0 = sig_an.get_n0()	# Starting index
	ixL = int(ceil(-Fs*(n0+0.5)/float(FB)))		# Left index for time axis
	ixR = int(ceil(Fs*(n0+N-0.5)/float(FB))) 	# Right index for time axis
	tt = arange(ixL,ixR)/float(Fs) 				# Time axis for s(t)
	t0 = tt[0]									# Start time for s(t)
	# print("s(t) time axis: ixL ", ixL, "ixR ", ixR, "len(tt)", len(tt))

	# ***** Conversion from DT a_n to CT a_s(t) *****
	an = sig_an.signal()		# Sequence a_n

	# ***** Display as polar binary ***** TODO Why does this work for multiple layers?
	if polarity == 'polar':
		zero_val_indices = an < .5
		an[zero_val_indices] = -1
		polarity = 'Polar'	# Set to capital for plot display

	ast = zeros(len(tt))		# Initialize a_s(t)
	ix = array(around(Fs*arange(0,N)/float(FB)),int)	# Symbol center indexes
	ast[ix-int(ixL)] = Fs*an	# delta_n -> delta(t) conversion
	# print("len(ast)", len(ast), "len(an)", len(an))

	# ***** Set up PAM pulse p(t) *****
	ptype = ptype.lower()		# Convert ptype to lowercase

	# Set left/right limits for p(t)
	if (ptype=='rect'):
		kL = -0.5; kR = -kL
	elif(ptype == 'tri'):
		kL = -1.0; kR = -kL
	elif(ptype == 'man'):
		kL = -0.5; kR = -kL
	elif (ptype == 'sinc') or (ptype == 'rcf'):
		kL = -pparms[0]; kR = -kL
	else:
		kL = -1.0; kR = -kL		# Default left/right limits

	ixpL = int(ceil(Fs*kL/float(FB)))		# Left index for p(t) time axis
	ixpR = int(ceil(Fs*kR/float(FB)))		# Right index for p(t) time axis
	ttp = arange(ixpL,ixpR)/float(Fs) 	# Time axis for p(t)
	pt = zeros(len(ttp))				# Initialize pulse p(t)
	# print("p(t) time axis: ixpL ", ixpL, "ixpR ", ixpR, "len(ttp)", len(ttp))

	m = int(Fs/float(FB))		# Upsampling multiplier factor
	sps = int(Fs/FB) 			# Number of time samples/bit
	# print("Fs: ", Fs, "FB: ", FB, "Fs/FB(sps): ", sps)

	# Generate an time axis for display of binary data
	if ptype == 'rect' or ptype == 'tri' or ptype == 'man':
		tan = arange(0, len(an))/float(FB)
	elif ptype == 'sinc' or ptype == 'rcf':
		tan = arange(ixL,ixR, sps)/float(Fs)


	# ***** Generate Pulse pt *****
	if (ptype=='rect'):		# Rectangular p(t)
		ix = where(logical_and(ttp>=kL/float(FB), ttp<kR/float(FB)))[0]
		pt[ix] = ones(len(ix))
	elif ptype == 'sinc': 	# Do the same thing for now..
		if m == 1:		# If no upsampling, new signal s(t) just = an
			ast = an
		else:			# If upsampling, add zeros
			ast = an
			for i in range(0,(m-1)):
				ast = vstack([ast, zeros(len(an))])   # We need M-1 rows of zeros
			ast = ast.flatten('F')
		pt = np.sinc(FB*ttp)
		pt = pt*kaiser(len(pt), pparms[1]) 			# Pulse p(t), Kaiser windowedy
	elif ptype == 'man':
		ix1  = where(logical_and(ttp>=kL/float(FB), ttp<0))[0]
		if polarity == 'Polar':
			pt[ix1] = -ones(len(ix1))
		ix2 = where(logical_and(ttp>=0, ttp < kR/float(FB)))[0]
		pt[ix2] = ones(len(ix2))
	elif ptype == 'tri':	# Do nothing because don't need to
		slope = -kL/float(sps)
		# print('slope: ', slope)
		bound = -ixpL
		j = 0
		for i in range(0, bound):
			pt[i] = j*slope
			j += 1
		j = sps
		for i in range(bound, 2*bound):
			pt[i] = j*slope
			j -= 1
	elif ptype == 'rcf':
		if m == 1:		# If no upsampling, new signal s(t) just = an
			ast = an
		else:			# If upsampling, add zeros
			ast = an
			for i in range(0,(m-1)):
				ast = vstack([ast, zeros(len(an))])   # We need M-1 rows of zeros
			ast = ast.flatten('F')
		[tht, pt] = sinc_ipol.rcf(ttp, FB, pparms[0], pparms[1])	# Generate rcf pulse
	else:
		print("ptype '%s' is not recognized" % ptype)

	# ***** Filter with h(t) = p(t) *****
	st = convolve(ast,pt)/float(Fs) 	# s(t) = a_s(t)*p(t)
	st = st[-ixpL:ixR-ixL-ixpL] 		# Trim after convolution
	# print("len(st):", len(st), 'len(tt):', len(tt))

	# ***** Plot *****
	if splot > 0:
		figure()
		if ptype == 'rect':
			plot(tt, st, tan, an, 'or'), grid()
			wave = 'Rectangular'
		elif ptype == 'tri':
			plot(tt, st, tan, an, 'or'), grid()
			wave = 'Triangular'
		elif ptype == 'man':
			plot(tt, st, tan, an, 'or'), grid()
			wave = 'Manchester'
		elif ptype == 'sinc':
			plot(tt, st*float(Fs), tan, an, 'or'), grid()
			wave = 'Sinc'
		elif ptype == 'rcf':
			plot(tf, st*(float(Fs)), tan, an, 'or'), grid()
			wave = 'RCF'
		xlabel('t[sec]')
		ylabel('s(t), s(nT_B)')
		if ptype == 'sinc':
			title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/beta$={:d}'.format(polarity, FB, wave, pparms[0], pparms[1]))
		elif ptype == 'rcf':
			title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/alpha$={:f}'.format(polarity, FB, wave, pparms[0], pparms[1]))
		else:
			title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t)'.format(polarity, FB, wave))
		show()

	if splot > 1:
		figure()
		plot(ttp, pt), grid()
		title('{:s} Pulse P(t)'.format(wave))
		show()
	return ecen.sigWave(st, Fs, t0) 	# Return waveform from sigWave class
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def pam12(sig_an, Fs, ptype, polarity, splot, pparms=[]):
	"""
	Pulse amplitude modulation: a_n -> s(t), -TB/2<=t<(N-1/2)*TB,
	V1.2 for 'man', 'rcf', 'rect', 'sinc', 'tri' and 'rrcf' pulse types.
	>>>>> sig_st = pam12(sig_an, Fs, ptype, pparms) <<<<<
	where 		sig_an: 			sequence from class sigSequ
				sig_an.signal(): 	N-symbol DT input sequence a_n, 0 <= n < N
				sig_an.get_FB(): 	Baud rate of a_n, TB=1/FB
				Fs:	 				sampling rate of s(t)
				ptype: 				pulse type from list ('man','rcf','rect','sinc','tri')
				pparms not used for 'man','rect','tri'
				pparms = [k, alpha] for 'rcf'
				pparms = [k, beta] for 'sinc'
				k:					"tail" truncation parameter for 'rcf','sinc'
									(truncates p(t) to -k*TB <= t < k*TB)
				alpha: 				Rolloff parameter for 'rcf', 0<=alpha<=1
				beta: 				Kaiser window parameter for 'sinc'
				sig_st: 			waveform from class sigWave
				sig_st.timeAxis(): 	time axis for s(t), starts at -TB/2
				sig_st.signal():	CT output signal s(t), -TB/2<=t<(N-1/2)*TB,
									with sampling rate Fs
				splot:				If splot > 0, plot the sig_st convolution
									result. IF splot > 1, plot the pulse in
									addition
	"""
	N = len(sig_an)			# Number of data symbols
	FB = sig_an.get_FB()	# Baud rate
	n0 = sig_an.get_n0()	# Starting index
	ixL = int(ceil(-Fs*(n0+0.5)/float(FB)))		# Left index for time axis
	ixR = int(ceil(Fs*(n0+N-0.5)/float(FB))) 	# Right index for time axis
	tt = arange(ixL,ixR)/float(Fs) 				# Time axis for s(t)
	t0 = tt[0]									# Start time for s(t)

	# ***** Conversion from DT a_n to CT a_s(t) *****
	an = sig_an.signal()		# Sequence a_n

	# ***** Display as polar binary ***** TODO Why does this work for multiple layers?
	if polarity == 'polar':
		zero_val_indices = an < .5
		an[zero_val_indices] = -1
		polarity = 'Polar'	# Set to capital for plot display

	ast = zeros(len(tt))		# Initialize a_s(t)
	ix = array(around(Fs*arange(0,N)/float(FB)),int)	# Symbol center indexes
	ast[ix-int(ixL)] = Fs*an	# delta_n -> delta(t) conversion

	# ***** Set up PAM pulse p(t) *****
	ptype = ptype.lower()		# Convert ptype to lowercase

	# Set left/right limits for p(t)
	if (ptype=='rect'):
		kL = -0.5; kR = -kL
	elif(ptype == 'tri'):
		kL = -1.0; kR = -kL
	elif(ptype == 'man'):
		kL = -0.5; kR = -kL
	elif (ptype == 'sinc') or (ptype == 'rcf') or (ptype == 'rrcf'):
		kL = -pparms[0]; kR = -kL
	else:
		kL = -1.0; kR = -kL		# Default left/right limits

	ixpL = int(ceil(Fs*kL/float(FB)))		# Left index for p(t) time axis
	ixpR = int(ceil(Fs*kR/float(FB)))		# Right index for p(t) time axis
	ttp = arange(ixpL,ixpR)/float(Fs) 	# Time axis for p(t)
	pt = zeros(len(ttp))				# Initialize pulse p(t)

	m = int(Fs/float(FB))		# Upsampling multiplier factor
	sps = int(Fs/FB) 			# Number of time samples/bit

	# Generate an time axis for display of binary data
	tan = arange(0, len(an))/float(FB)

	# ***** Generate Pulse pt *****
	if (ptype=='rect'):		# Rectangular p(t)
		ix = where(logical_and(ttp>=kL/float(FB), ttp<kR/float(FB)))[0]
		pt[ix] = ones(len(ix))
		wave = 'Rectangular'
	elif ptype == 'sinc': 	# Do the same thing for now..
		pt = np.sinc(FB*ttp)
		pt = pt*kaiser(len(pt), pparms[1]) 			# Pulse p(t), Kaiser windowedy
		wave = 'Sinc'
	elif ptype == 'man':
		ix1  = where(logical_and(ttp>=kL/float(FB), ttp<0))[0]
		if polarity == 'Polar':
			pt[ix1] = -ones(len(ix1))
		ix2 = where(logical_and(ttp>=0, ttp < kR/float(FB)))[0]
		pt[ix2] = ones(len(ix2))
		wave = 'Manchester'
	elif ptype == 'tri':	# Do nothing because don't need to
		slope = -kL/float(sps)
		# print('slope: ', slope)
		bound = -ixpL
		j = 0
		for i in range(0, bound):
			pt[i] = j*slope
			j += 1
		j = sps
		for i in range(bound, 2*bound):
			pt[i] = j*slope
			j -= 1
		wave = 'Triangular'
	elif ptype == 'rcf':
		[tht, pt] = sinc_ipol.rcf(ttp, FB, pparms[0], pparms[1])	# Generate rcf pulse
		wave = 'RCF'
	elif ptype == 'rrcf':
		TB = 1/float(FB)
		alpha = pparms[1]
		trange = TB/(4.0*alpha)
		# Get middle band between -trange< ixmid < trange and eliminate the
		#	index where ttp = 0
		ixmid = where(logical_and(logical_and(ttp>-trange, ttp<trange),ttp !=0))[0]
		# Get index where ttp = 0
		ix0 = where(ttp == 0)[0]
		# Get outside band s.t. ixout < -trange & ixout > trange
		ixout = where(logical_or(ttp>=trange, ttp<-trange))[0]
		pt[ix0] = 1.0-alpha+(4.0*alpha)/pi
		pt[ixmid] = TB/pi*(sin((1-alpha)*pi*ttp[ixmid]/TB)+(4.0*alpha*ttp[ixmid]/TB)*cos((1+alpha)*pi*ttp[ixmid]/TB))/((1.0-(4.0*alpha*ttp[ixmid]/TB)**2.0)*ttp[ixmid])
		pt[ixout] = TB/pi*(sin((1-alpha)*pi*ttp[ixout]/TB)+(4.0*alpha*ttp[ixout]/TB)*cos((1+alpha)*pi*ttp[ixout]/TB))/((1.0-(4.0*alpha*ttp[ixout]/TB)**2.0)*ttp[ixout])
		wave = 'RRCF'
	else:
		print("ptype '%s' is not recognized" % ptype)

	# ***** Filter with h(t) = p(t) *****
	st = convolve(ast,pt)/float(Fs) 	# s(t) = a_s(t)*p(t)
	st = st[-ixpL:ixR-ixL-ixpL] 		# Trim after convolution

	# ***** Plot *****
	if splot > 0:
		figure()
		plot(tt, st, tan, an, 'or'), grid()
		xlabel('t[sec]')
		ylabel('s(t), s(nT_B)')
		if ptype == 'sinc':
			title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/beta$={:d}'.format(polarity, FB, wave, pparms[0], pparms[1]))
		elif ptype == 'rcf':
			title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/alpha$={:f}'.format(polarity, FB, wave, pparms[0], pparms[1]))
		else:
			title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t)'.format(polarity, FB, wave))
		show()

	if splot > 1:
		figure()
		plot(ttp, pt), grid()
		title('{:s} Pulse P(t)'.format(wave))
		show()
	return ecen.sigWave(st, Fs, t0) 	# Return waveform from sigWave class
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def pam12_2(sig_an, Fs, ptype, splot, pparms=[]):
	"""
	Pulse amplitude modulation: a_n -> s(t), -TB/2<=t<(N-1/2)*TB,
	V1.2.2 for 'man', 'rcf', 'rect', 'sinc', 'tri' and 'rrcf' pulse types.
	>>>>> sig_st = pam12(sig_an, Fs, ptype, pparms) <<<<<
	where 		sig_an: 			sequence from class sigSequ
				sig_an.signal(): 	N-symbol DT input sequence a_n, 0 <= n < N
				sig_an.get_FB(): 	Baud rate of a_n, TB=1/FB
				Fs:	 				sampling rate of s(t)
				ptype: 				pulse type from list ('man','rcf','rect','sinc','tri')
				pparms not used for 'man','rect','tri'
				pparms = [k, alpha] for 'rcf'
				pparms = [k, beta] for 'sinc'
				k:					"tail" truncation parameter for 'rcf','sinc'
									(truncates p(t) to -k*TB <= t < k*TB)
				alpha: 				Rolloff parameter for 'rcf', 0<=alpha<=1
				beta: 				Kaiser window parameter for 'sinc'
				sig_st: 			waveform from class sigWave
				sig_st.timeAxis(): 	time axis for s(t), starts at -TB/2
				sig_st.signal():	CT output signal s(t), -TB/2<=t<(N-1/2)*TB,
									with sampling rate Fs
				splot:				If splot > 0, plot the sig_st convolution
									result. IF splot > 1, plot the pulse in
									addition
	"""
	N = len(sig_an)			# Number of data symbols
	FB = sig_an.get_FB()	# Baud rate
	n0 = sig_an.get_n0()	# Starting index
	ixL = int(ceil(-Fs*(n0+0.5)/float(FB)))		# Left index for time axis
	ixR = int(ceil(Fs*(n0+N-0.5)/float(FB))) 	# Right index for time axis
	tt = arange(ixL,ixR)/float(Fs) 				# Time axis for s(t)
	t0 = tt[0]									# Start time for s(t)

	# ***** Conversion from DT a_n to CT a_s(t) *****
	an = sig_an.signal()		# Sequence a_n

	ast = zeros(len(tt))		# Initialize a_s(t)
	ix = array(around(Fs*arange(0,N)/float(FB)),int)	# Symbol center indexes
	ast[ix-int(ixL)] = Fs*an	# delta_n -> delta(t) conversion

	# ***** Set up PAM pulse p(t) *****
	ptype = ptype.lower()		# Convert ptype to lowercase

	# Set left/right limits for p(t)
	if (ptype=='rect'):
		kL = -0.5; kR = -kL
	elif(ptype == 'tri'):
		kL = -1.0; kR = -kL
	elif(ptype == 'man'):
		kL = -0.5; kR = -kL
	elif (ptype == 'sinc') or (ptype == 'rcf') or (ptype == 'rrcf'):
		kL = -pparms[0]; kR = -kL
	else:
		kL = -1.0; kR = -kL		# Default left/right limits

	ixpL = int(ceil(Fs*kL/float(FB)))		# Left index for p(t) time axis
	ixpR = int(ceil(Fs*kR/float(FB)))		# Right index for p(t) time axis
	ttp = arange(ixpL,ixpR)/float(Fs) 	# Time axis for p(t)
	pt = zeros(len(ttp))				# Initialize pulse p(t)

	m = int(Fs/float(FB))		# Upsampling multiplier factor
	sps = int(Fs/FB) 			# Number of time samples/bit

	# Generate an time axis for display of binary data
	tan = arange(0, len(an))/float(FB)

	# ***** Generate Pulse pt *****
	if (ptype=='rect'):		# Rectangular p(t)
		ix = where(logical_and(ttp>=kL/float(FB), ttp<kR/float(FB)))[0]
		pt[ix] = ones(len(ix))
		wave = 'Rectangular'
	elif ptype == 'sinc': 	# Do the same thing for now..
		pt = np.sinc(FB*ttp)
		pt = pt*kaiser(len(pt), pparms[1]) 			# Pulse p(t), Kaiser windowedy
		wave = 'Sinc'
	elif ptype == 'man':
		ix1  = where(logical_and(ttp>=kL/float(FB), ttp<0))[0]

		ix2 = where(logical_and(ttp>=0, ttp < kR/float(FB)))[0]
		pt[ix2] = ones(len(ix2))
		wave = 'Manchester'
	elif ptype == 'tri':	# Do nothing because don't need to
		slope = -kL/float(sps)
		# print('slope: ', slope)
		bound = -ixpL
		j = 0
		for i in range(0, bound):
			pt[i] = j*slope
			j += 1
		j = sps
		for i in range(bound, 2*bound):
			pt[i] = j*slope
			j -= 1
		wave = 'Triangular'
	elif ptype == 'rcf':
		[tht, pt] = sinc_ipol.rcf(ttp, FB, pparms[0], pparms[1])	# Generate rcf pulse
		wave = 'RCF'
	elif ptype == 'rrcf':
		TB = 1/float(FB)
		alpha = pparms[1]
		trange = TB/(4.0*alpha)
		# Get middle band between -trange< ixmid < trange and eliminate the
		#	index where ttp = 0
		ixmid = where(logical_and(logical_and(ttp>-trange, ttp<trange),ttp !=0))[0]
		# Get index where ttp = 0
		ix0 = where(ttp == 0)[0]
		# Get outside band s.t. ixout < -trange & ixout > trange
		ixout = where(logical_or(ttp>=trange, ttp<-trange))[0]
		pt[ix0] = 1.0-alpha+(4.0*alpha)/pi
		pt[ixmid] = TB/pi*(sin((1-alpha)*pi*ttp[ixmid]/TB)+(4.0*alpha*ttp[ixmid]/TB)*cos((1+alpha)*pi*ttp[ixmid]/TB))/((1.0-(4.0*alpha*ttp[ixmid]/TB)**2.0)*ttp[ixmid])
		pt[ixout] = TB/pi*(sin((1-alpha)*pi*ttp[ixout]/TB)+(4.0*alpha*ttp[ixout]/TB)*cos((1+alpha)*pi*ttp[ixout]/TB))/((1.0-(4.0*alpha*ttp[ixout]/TB)**2.0)*ttp[ixout])
		wave = 'RRCF'
	else:
		print("ptype '%s' is not recognized" % ptype)

	# ***** Filter with h(t) = p(t) *****
	st = convolve(ast,pt)/float(Fs) 	# s(t) = a_s(t)*p(t)
	st = st[-ixpL:ixR-ixL-ixpL] 		# Trim after convolution

	# ***** Plot *****
	if splot > 0:
		figure()
		plot(tt, st, tan, an, 'or'), grid()
		xlabel('t[sec]')
		ylabel('s(t), s(nT_B)')
		if ptype == 'sinc':
			title('Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/beta$={:d}'.format(FB, wave, pparms[0], pparms[1]))
		elif ptype == 'rcf':
			title('Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/alpha$={:f}'.format(FB, wave, pparms[0], pparms[1]))
		else:
			title('Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t)'.format(FB, wave))
		show()

	if splot > 1:
		figure()
		plot(ttp, pt), grid()
		title('{:s} Pulse P(t)'.format(wave))
		show()
	return ecen.sigWave(st, Fs, t0) 	# Return waveform from sigWave class
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def pamrcvr10(sig_rt, FBparms, ptype, splot, pparms=[]):
	"""
	Pulse amplitude modulation receiver with matched filter: r(t) -> b(t) -> bn.
	V1.0 for 'man', 'rcf', 'rect', 'rrcf', 'sinc', and 'tri' pulse types.
	>>>>> sig_bn, sig_bt, ixn = pamrcvr10(sig_rt, FBparms, ptype, pparms) <<<<<
	where 	sig_rt: 			waveform from class sigWave
			sig_rt.signal():	received (noisy) PAM signal r(t)
			sig_rt.timeAxis(): 	time axis for r(t)
			FBparms: 			= [FB, dly]
			FB:					Baud rate of PAM signal, TB=1/FB
			dly:				sampling delay for b(t) -> b_n as a fraction of TB
								sampling times are t=n*TB+t0 where t0 = dly*TB
			ptype: 				pulse type from list
								('man','rcf','rect','rrcf','sinc','tri')
								pparms not used for 'man','rect','tri'
								pparms = [k, alpha] for 'rcf','rrcf'
								pparms = [k, beta] for 'sinc'
			k:					"tail" truncation parameter for 'rcf','rrcf','sinc'
								(truncates p(t) to -k*TB <= t < k*TB)
			alpha: 				rolloff parameter for ('rcf','rrcf'), 0<=alpha<=1
			beta: 				Kaiser window parameter for 'sinc'
			sig_bn: 			sequence from class sigSequ
			sig_bn.signal(): 	received DT sequence after sampling at t=n*TB+t0
			sig_bt: 			waveform from class sigWave
			sig_bt.signal(): 	received PAM signal b(t) at output of matched filter
			ixn:				indexes where b(t) is sampled to obtain b_n
	"""
	# polarity = 'polar'
	if type(FBparms)==int or type(FBparms)==float:
		FB, t0 = FBparms, 0		# Get FBparms parameters
	else:
		FB, t0 = FBparms[0],0
		if len(FBparms) > 1:
			t0 = FBparms[1]/float(FB)

	Fs = sig_rt.get_Fs()		# Sampling rate
	rt = sig_rt.signal()		# Received signal r(t)
	tt = sig_rt.timeAxis()		# Time axis for r(t)
	nn0 = int(ceil((tt[0]-t0)*FB)) 	# First data index
									# Integer multiple of 1/FB
	ixnn0 = argmin(abs(tt-(nn0/float(FB)+t0)))
	N = int(floor((tt[-1]-tt[ixnn0])*FB)) + 1 	# Number of data symbols

	# ***** Set up matched filter response h_R(t) *****
	ptype = ptype.lower()		# Convert ptype to lowercase
								# Set left/right limits for p(t)
	if (ptype=='rect'):			# Rectangular p(t)
		kL = -0.5; kR = -kL
	elif(ptype == 'tri'):
		kL = -1.0; kR = -kL
	elif(ptype == 'man'):
		kL = -0.5; kR = -kL
	elif (ptype == 'sinc') or (ptype == 'rcf') or (ptype == 'rrcf'):
		kL = -pparms[0]; kR = -kL
	else:
		kL = -1.0; kR = -kL		# Default left/right limits

	ixpL = int(ceil(Fs*kL/float(FB))) # Left index for p(t) time axis
	ixpR = int(ceil(Fs*kR/float(FB))) # Right index for p(t) time axis
	ttp = arange(ixpL,ixpR)/float(Fs) # Time axis for p(t)
	pt = zeros(len(ttp))		# Initialize pulse p(t)

	# ***** Generate Pulse pt *****
	if (ptype=='rect'):			# Rectangular p(t)
		ix = where(logical_and(ttp>=kL/float(FB), ttp<kR/float(FB)))[0]
		pt[ix] = ones(len(ix))
		wave = 'Rectangular'
	elif ptype == 'sinc': 	# Do the same thing for now..
		pt = np.sinc(FB*ttp)
		pt = pt*kaiser(len(pt), pparms[1]) 			# Pulse p(t), Kaiser windowedy
		wave = 'Sinc'
	elif ptype == 'man':
		ix1  = where(logical_and(ttp>=kL/float(FB), ttp<0))[0]
		# if polarity == 'Polar':
			# pt[ix1] = -ones(len(ix1))
		ix2 = where(logical_and(ttp>=0, ttp < kR/float(FB)))[0]
		pt[ix2] = ones(len(ix2))
		wave = 'Manchester'
	elif ptype == 'tri':	# Do nothing because don't need to
		slope = -kL/float(sps)
		# print('slope: ', slope)
		bound = -ixpL
		j = 0
		for i in range(0, bound):
			pt[i] = j*slope
			j += 1
		j = sps
		for i in range(bound, 2*bound):
			pt[i] = j*slope
			j -= 1
		wave = 'Triangular'
	elif ptype == 'rcf':
		[tht, pt] = sinc_ipol.rcf(ttp, FB, pparms[0], pparms[1])	# Generate rcf pulse
		wave = 'RCF'
	elif ptype == 'rrcf':
		TB = 1/float(FB)
		alpha = pparms[1]
		trange = TB/(4.0*alpha)
		# Get middle band between -trange< ixmid < trange and eliminate the
		#	index where ttp = 0
		ixmid = where(logical_and(logical_and(ttp>-trange, ttp<trange),ttp !=0))[0]
		# Get index where ttp = 0
		ix0 = where(ttp == 0)[0]
		# Get outside band s.t. ixout < -trange & ixout > trange
		ixout = where(logical_or(ttp>=trange, ttp<-trange))[0]
		pt[ix0] = 1.0-alpha+(4.0*alpha)/pi
		pt[ixmid] = TB/pi*(sin((1-alpha)*pi*ttp[ixmid]/TB)+(4.0*alpha*ttp[ixmid]/TB)*cos((1+alpha)*pi*ttp[ixmid]/TB))/((1.0-(4.0*alpha*ttp[ixmid]/TB)**2.0)*ttp[ixmid])
		pt[ixout] = TB/pi*(sin((1-alpha)*pi*ttp[ixout]/TB)+(4.0*alpha*ttp[ixout]/TB)*cos((1+alpha)*pi*ttp[ixout]/TB))/((1.0-(4.0*alpha*ttp[ixout]/TB)**2.0)*ttp[ixout])
		wave = 'RRCF'
	else:
		print("ptype '%s' is not recognized" % ptype)

	hRt = pt[::-1]			# h_R(t) = p(-t)
	hRt = Fs/sum(np.power(pt,2.0))*hRt # h_R(t) normalized

	# ***** Filter r(t) with matched filter h_R(t) *****
	bt = convolve(rt,hRt)/float(Fs) # Matched filter b(t)=r(t)*h_R(t)
	bt = bt[-ixpL:len(tt)-ixpL] # Trim b(t)

	# ***** Sample b(t) at t=n*TB+t0 to obtain b_n *****
	ixn = ixnn0 + array(around(Fs*arange(N)/float(FB)),int)	# Sampling indexes
	bn = bt[ixn]				# DT sequence sampled at t=n*TB+t0

	if splot[0] > 0:
		strttl = 'Filtered Output| {:s} pulse | Delay = {:f}'.format(wave, FBparms[1])
		f1 = figure()
		af1 = f1.add_subplot(111)
		af1.plot(tt, bt)
		af1.plot(tt[ixn], bt[ixn], 'or')
		af1.set_title(strttl)
		af1.set_xlim(0, splot[1])
		show()
	return ecen.sigSequ(bn,FB,nn0), ecen.sigWave(bt,Fs,tt[0]), ixn

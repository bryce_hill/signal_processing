# File: ftpam01.py
# Script file that accepts an ASCII text string as input and
# produces a corresponding binary unipolar flat-top PAM signal
# s(t) with bit rate Fb and sampling rate Fs.
# The ASCII text string uses 8-bit encoding and LSB-first
# conversion to a bitstream dn. At every index value
# n=0,1,2,..., dn is either 0 or 1. To convert dn to a
# flat-top PAM CT signal approximation s(t), the formula
# 	s(t) = dn, (n-1/2)*Tb <= t < (n+1/2)*Tb,
# is used.

from pylab import *
import my_ascbin.ascfun as af

def ftpam(Fs = 44100, Fb = 100, txt='MyTest'):
	# Fs = 44100		# Sampling rate for s(t)
	# Fb = 100			# Bit rate for dn sequence
	# txt = 'MyTest'	# Input text string

	bits = 8				# Number of bits
	dn = af.asc2bin(txt)	# >> Convert txt to bitstream dn here <<
	N = len(dn)				# Total number of bits

	Tb = 1/float(Fb)				# Time per bit
	ixL = round(-0.5*Fs*Tb)			# Left index for time axis
	ixR = round((N-0.5)*Fs*Tb) 		# Right index for time axis
	tt = arange(ixL,ixR)/float(Fs) 	# Time axis for s(t)

	numTpBit = len(tt)/N     # Number of time intervals per bit

	st = zeros(len(tt))      # Create array to hold s(t)
	for i in range (0,len(st)): # Loop over array s(t) to fill it
	    index = int(i/numTpBit) # Get index of next bit based on # of time intervals/bit
	    st[i] = dn[index]       # Populate s(t)

	return tt, st

# File: filtfun.py
# Module for filter functions
from pylab import *
from scipy.signal import butter, lfilter
import my_sdr as sdr
import my_sinecos as sc
import numpy as np

def trapfilt0(sig_xt, fL, k, alpha, splot, limit):
	"""
	Delay compensated FIR LPF/BPF filter with trapezoidal
	frequency response.
	>>>>> sig_yt, n = trapfilt(sig_xt, fL, k, alpha) <<<<<
	where sig_yt: 		waveform from class sigWave
	sig_yt.signal(): 	filter output y(t), samp rate Fs
	n:				 	filter order
	sig_xt: 			waveform from class sigWave
	sig_xt.signal(): 	filter input x(t), samp rate Fs
	sig_xt.get_Fs(): 	sampling rate for x(t), y(t)
	fL:					LPF cutoff frequency (-6 dB) in Hz
	k:					h(t) is truncated to |t| <= k/(2*fL)
	alpha:				frequency rolloff parameter, linear rolloff
						over range (1-alpha)fL <= |f| <= (1+alpha)fL
	"""
	xt = sig_xt.signal()	 # Input signal
	Fs = sig_xt.get_Fs()	 # Sampling rate
	ixk = round(Fs*k/float(2*fL)) 			# Tail cutoff index
	tth = arange(-ixk,ixk+1)/float(Fs) 		# Time axis for h(t)
	n = len(tth)-1			 # Filter order

	# ***** Generate impulse response ht here *****
	ht = zeros(len(tth))		# Generate ht of correct size
	# Logical and out the index where tth = 0 as this will break the function
	ix0 = where(tth == 0)[0]
	ix = where(logical_and(logical_and(tth>tth[0], tth<tth[len(tth)-1]),tth !=0))[0]
	ht[ix] = sin(2.0*pi*fL*tth[ix])/(1.0*pi*tth[ix])
	ht[ix] = ht[ix]*sin(2.0*pi*alpha*fL*tth[ix])/(2.0*pi*alpha*fL*tth[ix])
	ht[ix0] = fL*2				# Set value for tth = 0 case

	# ***** Calculate filter output *****
	yt = lfilter(ht, 1, hstack((xt, zeros(ixk))))/float(Fs)		# Compute filter output y(t)
	yt = yt[ixk:]
	ty = arange(0, len(yt))/float(Fs)

	# ***** If plotting requested, plot ht *****
	if splot > 0:
		# f1 = figure()
		f1 = figure()
		p1 = f1.add_subplot(211)
		p1.plot(ty, yt), grid()
		p1.set_title('Filter Output')
		p1.set_xlabel('Time [s]')
		p2 = f1.add_subplot(212)
		p2.plot(tth, ht), grid()
		p2.set_title('Pulse h(t)')
		p2.set_xlabel('Time [s]')
		if limit != 0:
			p1.set_xbound(0, limit)
		show()

	return sdr.sigWave(yt, Fs, sig_xt.get_t0()), n				# Return y(t) and filter order

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def trapfilt(sig_xt, fparms, k, alpha, splot, limit):
	"""
	Delay compensated FIR LPF/BPF filter with trapezoidal
	frequency response.
	>>>>> sig_yt, n = trapfilt(sig_xt, fparms, k, alpha) <<<<<
	where
	sig_yt:	 			waveform from class sigWave
	sig_yt.signal(): 	filter output y(t), samp rate Fs
	n:					filter order
	sig_xt: 			waveform from class sigWave
	sig_xt.signal(): 	filter input x(t), samp rate Fs
	sig_xt.get_Fs(): 	sampling rate for x(t), y(t)
	fparms: 			= fL for LPF
	fL:					LPF cutoff frequency (-6 dB) in Hz
	fparms: 			= [fBW, fc] for BPF
	fBW: 				BPF -6dB bandwidth in Hz
	fc:					BPF center frequency in Hz
	k:					h(t) is truncated to
						|t| <= k/(2*fL) for LPF
						|t| <= k/fBW for BPF
	alpha: 				frequency rolloff parameter, linear
						rolloff over range
						(1-alpha)fL <= |f| <= (1+alpha)fL for LPF
						(1-alpha)fBW/2 <= |f| <= (1+alpha)fBW/2 for LPF
	"""
	xt = sig_xt.signal()	# Input signal
	Fs = sig_xt.get_Fs()	# Sampling rate
	if fparms[1] == 0: 		# LPF
		fL = fparms[0]
		ixk = round(Fs*k/float(2*fL)) 			# Tail cutoff index
	else:					# BPF
		fBW = fparms[0]
		fL = fBW/2.0
		fc = fparms[1]
		ixk = round(Fs*k/float(2*fBW)) 			# Tail cutoff index
	tth = arange(-ixk,ixk+1)/float(Fs) 			# Time axis for h(t)
	n = len(tth)-1			# Filter order

	# ***** Generate impulse response ht here *****
	ht = zeros(len(tth))		# Generate ht of correct size
	# Logical and out the index where tth = 0 as this will break the function
	ix0 = where(tth == 0)[0]	# Not necessary when I use the np.sinc function instead of writing it all out with sines, I guess numpy takes care of the zero case
	ix = where(logical_and(logical_and(tth>tth[0], tth<tth[len(tth)-1]),tth !=0))[0]

	# ht[ix] = sin(2.0*pi*fL*tth[ix])/(1.0*pi*tth[ix])
	# ht[ix] = ht[ix]*sin(2.0*pi*alpha*fL*tth[ix])/(2.0*pi*alpha*fL*tth[ix])
	ht = 2*fL*np.sinc(2*fL*tth)*np.sinc(2*alpha*fL*tth)
	if fparms[1] != 0:			# BPF
		ht = 2*ht*cos(2*pi*fc*tth)	# Frequency shift
	# ht[ix0] = fL*2				# Set value for tth = 0 case

	# ***** Calculate filter output *****
	yt = lfilter(ht, 1, hstack((xt, zeros(ixk))))/float(Fs)		# Compute filter output y(t)
	yt = yt[ixk:]
	ty = arange(0, len(yt))/float(Fs)

	# ***** If plotting requested, plot ht *****
	if splot > 0:
		f1 = figure()
		p1 = f1.add_subplot(211)
		p1.plot(ty, yt), grid()
		p1.set_title('Filter Output')
		p1.set_xlabel('Time [s]')
		p2 = f1.add_subplot(212)
		p2.plot(tth, ht), grid()
		p2.set_title('Pulse h(t)')
		p2.set_xlabel('Time [s]')
		if limit != 0:
			p1.set_xbound(0, limit)
			p2.set_xbound(-limit, limit)
		show()

	return sdr.sigWave(yt, Fs, sig_xt.get_t0()), n				# Return y(t) and filter order

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def trapfilt_cc(sig_xt, fparms, k, alpha, splot, limit):
	"""
	Delay compensated FIR LPF/BPF filter with trapezoidal
	frequency response, complex-valued input/output and
	complex-valued filter coefficients.
	>>>>> sig_yt, n = trapfilt_cc(sig_xt, fparms, k, alpha) <<<<<
	where
	sig_yt: 			waveform from class sigWave
	sig_yt.signal():	complex filter output y(t), samp rate Fs
	n:					filter order
	sig_xt: 			waveform from class sigWave
	sig_xt.signal():	complex filter input x(t), samp rate Fs
	sig_xt.get_Fs():	sampling rate for x(t), y(t)
	fparms:				= fL for LPF
	fL:					LPF cutoff frequency (-6 dB) in Hz
	fparms: 			= [fBW, fBc] for BPF
	fBW: 				BPF -6dB bandwidth in Hz
	fBc:				BPF center frequency (pos/neg) in Hz
	k:					h(t) is truncated to
						|t| <= k/(2*fL) for LPF
						|t| <= k/fBW for BPF
	alpha: 				frequency rolloff parameter, linear
						rolloff over range
						(1-alpha)*fL <= |f| <= (1+alpha)*fL for LPF
						(1-alpha)*fBW/2 <= |f| <= (1+alpha)*fBW/2 for BPF
	"""
	xt = sig_xt.signal()	# Input signal
	Fs = sig_xt.get_Fs()	# Sampling rate
	txt = sig_xt.timeAxis() # Get time axis for xt

	if fparms[1] == 0: 		# LPF
		fL = fparms[0]
		ixk = round(Fs*k/float(2*fL)) 			# Tail cutoff index
	else:					# BPF
		fBW = fparms[0]
		fL = fBW/2.0
		fc = fparms[1]
		ixk = round(Fs*k/float(2*fBW)) 			# Tail cutoff index
	tth = arange(-ixk,ixk+1)/float(Fs) 		# Time axis for h(t)
	n = len(tth)-1			# Filter order

	# ***** Generate impulse response ht here *****
	# Logical_and out the index where tth = 0 as this will break the function
	ix0 = where(tth == 0)[0]
	ix = where(logical_and(logical_and(tth>tth[0], tth<tth[len(tth)-1]),tth !=0))[0]
	ht = zeros(len(tth)) + 1j*zeros(len(tth))		# Generate ht of correct size

	ht = 2*fL*np.sinc(2*fL*tth)*np.sinc(2*alpha*fL*tth)

	# ***** If bandpass, frequency shift ****
	if fparms[1] != 0:
		ht = ht*exp(1j*2*pi*fc*tth)

	# ***** Calculate filter output *****
	yt = lfilter(ht, 1, hstack((xt, zeros(ixk))))/float(Fs)		# Compute filter output y(t)
	yt = yt[ixk:]
	ty = arange(0, len(yt))/float(Fs)

	# ***** If plotting requested, plot ht *****
	if splot > 0:
		f1 = figure()
		p1 = f1.add_subplot(211)
		# f1.tight_layout()
		p1.plot(ty, yt), grid()
		p1.set_title('Filter Output')
		p1.set_xlabel('Time [s]')
		p2 = f1.add_subplot(212)
		p2.plot(tth, ht), grid()
		p2.set_title('Pulse h(t)')
		p2.set_xlabel('Time [s]')
		if limit != 0:
			p1.set_xbound(0, limit)
			p2.set_xbound(-limit, limit)
		show()

	return sdr.sigWave(yt, Fs, sig_xt.get_t0()), n				# Return y(t) and filter order

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def askrcvr(sig_rt,rtype,fcparms,FBparms,ptype,pparms):
	"""
	Amplitude Shift Keying (ASK) Receiver for
	Coherent ('coh') and Non-coherent ('noncoh') ASK Signals
	>>>>> sig_bn,sig_bt,sig_wt,ixn =
	askrcvr(sig_rt,rtype,fcparms,FBparms,ptype,pparms) <<<<<
	where
	sig_bn: 			sequence from class sigSequ
	sig_bn.signal(): 	received DT sequence b[n]
	sig_bt: 			waveform from class sigWave
	sig_bt.signal(): 	received 'CT' PAM signal b(t)
	sig_wt: 			waveform from class sigWave
	sig_wt.signal(): 	wt = wit + 1j*wqt
	wit:				in-phase component of b(t)
	wqt:				quadrature component of b(t)
	ixn:				sampling time indexes for b(t)->b[n], w(t)->w[n]
	sig_rt: 			waveform from class sigWave
	sig_rt.signal():	received (noisy) ASK signal r(t)
	sig_rt.timeAxis(): 	time axis for r(t)
	rtype:				receiver type from list ['coh','noncoh']
	fcparms = [fc, thetac] 	for {'coh'}
	fcparms = [fc]			for {'noncoh'}
	fc:					carrier frequency in Hz
	thetac:				carrier phase in deg (0: cos, -90: sin)
	FBparms = [FB, dly]
	FB:					baud rate of PAM signal, TB=1/FB
	dly:				sampling delay for b(t)->b[n], fraction of TB
						sampling times are t=n*TB+t0 where t0=dly*TB
	ptype:				pulse type from list
						['man','rcf','rect','rrcf','sinc','tri']
	pparms = [] 		for 'man','rect','tri'
	pparms = [k, alpha] for {'rcf','rrcf'}
	pparms = [k, beta]	for {'sinc'}
	k:					"tail" truncation parameter for {'rcf','rrcf','sinc'}
						(truncates at -k*TB and k*TB)
	alpha:				Rolloff parameter for {'rcf','rrcf'}, 0<=alpha<=1
	beta:				Kaiser window parameter for {'sinc'}
	"""

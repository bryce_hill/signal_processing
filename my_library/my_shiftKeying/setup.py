from setuptools import setup

setup(name="my_shiftKeying",
      version = "0.0",
      description = "Custom python modules for frequency and phase shift keying",
      author = "Bryce Hill",
      packages=["my_shiftKeying"],
)

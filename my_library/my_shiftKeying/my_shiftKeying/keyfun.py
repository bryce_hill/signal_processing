# File: keyfun.py
# Functions for amplitude/frequency/phase shift keying
# ASK, FSK, PSK and hybrid APSK
from pylab import *
import my_sdr as sdr
import my_ft as ft

def testmy_shiftKeying():
    return('Package shiftKeying imported correctly')

def askxmtr(sig_an,Fs,ptype,pparms,xtype,fcparms):
    """
    Amplitude Shift Keying (ASK) Transmitter for
    Coherent ('coh') and Non-coherent ('noncoh') ASK Signals
    >>>>> sig_xt,sig_st = askxmtr(sig_an,Fs,ptype,pparms,xtype,fcparms) <<<<<
    where
    sig_xt: 			waveform from class sigWave
    sig_xt.signal():	transmitted ASK signal, sampling rate Fs
    					x(t) = s(t)*cos(2*pi*fc*t+(pi/180)*thetac)
    sig_xt.timeAxis(): 	time axis for x(t), starts at t=-TB/2
    sig_st: 			waveform from class sigWave
    sig_st.signal(): 	baseband PAM signal s(t) for 'coh'
    sig_st.signal(): 	st = sit + 1j*sqt for 'noncoh'
    sit:				PAM signal of an*cos(pi/180*thetacn)
    sqt:				PAM signal of an*sin(pi/180*thetacn)
    xtype:				Transmitter type from list {'coh','noncoh'}
    sig_an: 			sequence from class sigSequ
    					sig_an.signal() = [an] for {'coh'}
    					sig_an.signal() = [[an],[thetacn]]
     					for {'noncoh'}
    an:					N-symbol DT input sequence a_n, 0<=n<N
    thetacn: 			N-symbol DT sequence theta_c[n] in degrees,
    					used instead of thetac for {'noncoh'} ASK
    sig_an.get_FB(): 	baud rate of a_n (and theta_c[n]), TB=1/FB
    Fs:					sampling rate of x(t), s(t)
    ptype:				pulse type from list
    					['man','rcf','rect','rrcf','sinc','tri']
    pparms = [] 		for {'man','rect','tri'}
    pparms = [k, alpha] for {'rcf','rrcf'}
    pparms = [k, beta]  for {'sinc'}
    k:					"tail" truncation parameter for {'rcf','rrcf','sinc'}
    					(truncates at -k*TB and k*TB)
    alpha:				Rolloff parameter for {'rcf','rrcf'}, 0<=alpha<=1
    beta:				Kaiser window parameter for {'sinc'}
    fcparms = [fc, thetac] 	for {'coh'}
    fcparms = [fc]			for {'noncoh'}
    fc:					carrier frequency in Hz
    thetac: 			carrier phase in deg (0: cos, -90: sin)
    """

    # ***** Randomize Phase if noncoh signal *****
    if xtype == 'noncoh':
        thetacn = 2*pi*random(len(sig_an))
        sig_ani = sdr.sigSequ(sig_an.sig*cos(thetacn), sig_an.get_FB(), 0)# delta_n -> delta(t) in-phase conversion
        sig_anq = sdr.sigSequ(sig_an.sig*sin(thetacn), sig_an.get_FB(), 0)# delta_n -> delta(t) quadrature conversion

    # ***** Generate PAM ***** TODO better descriptions
    if (xtype == 'noncoh'):
        sig_sti = sdr.pam12_2(sig_ani, Fs, ptype, 0, pparms)
        sig_stq = sdr.pam12_2(sig_anq, Fs, ptype, 0, pparms)
        t0 = sig_sti.get_t0()
    else:
        sig_st = sdr.pam12_2(sig_an, Fs, ptype, 0, pparms)
        t0 = sig_st.get_t0()

    fc = fcparms[0]
    if xtype == 'coh':
        thetac = fcparms[1]

    # ***** Multiply with oscillator *****
    if(xtype == 'noncoh'):
        xt = sig_sti.sig*cos(2*pi*fc*sig_sti.timeAxis()) + sig_stq.sig*-sin(2*pi*fc*sig_stq.timeAxis())
    else:   # 'coh'
        xt = sig_st.sig*cos(2*pi*fc*sig_st.timeAxis() + thetac)

    # ***** Plot *****
    """if splot > 0:
		figure()
		plot(tt, st, tan, an, 'or'), grid()
		xlabel('t[sec]')
		ylabel('s(t), s(nT_B)')
		if ptype == 'sinc':
			title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/beta$={:d}'.format(polarity, FB, wave, pparms[0], pparms[1]))
		elif ptype == 'rcf':
			title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t), k={:d}, $/alpha$={:f}'.format(polarity, FB, wave, pparms[0], pparms[1]))
		else:
			title('{:s} Binary PAM for String \'Test\', $FB$={:d} BAUD, {:s} p(t)'.format(polarity, FB, wave))
		show()

    if splot > 1:
		figure()
		plot(ttp, pt), grid()
		title('{:s} Pulse P(t)'.format(wave))
		show()
	"""
    return sdr.sigWave(xt, Fs, t0) 	# Return waveform from sigWave class

def askxmtr_plot(sig_xx, pset):
    """
    Amplitude Shift Keying (ASK) Receiver for
    Coherent ('coh') and Non-coherent ('noncoh') ASK Signals
    >>>>> sig_bn,sig_bt,sig_wt,ixn =
    askrcvr(sig_rt,rtype,fcparms,FBparms,ptype,pparms) <<<<<
    where
    sig_bn: 			sequence from class sigSequ
    sig_bn.signal(): 	received DT sequence b[n]
    sig_bt: 			waveform from class sigWave
    sig_bt.signal(): 	received 'CT' PAM signal b(t)
    sig_wt: 			waveform from class sigWave
    sig_wt.signal(): 	wt = wit + 1j*wqt
    wit:				in-phase component of b(t)
    wqt:				quadrature component of b(t)

    fcparms = [fc, thetac] 	for {'coh'}
    fcparms = [fc]			for {'noncoh'}
    fc:					carrier frequency in Hz
    thetac:				carrier phase in deg (0: cos, -90: sin)
    FBparms = [FB, dly]
    FB:					baud rate of PAM signal, TB=1/FB
    dly:				sampling delay for b(t)->b[n], fraction of TB
    					sampling times are t=n*TB+t0 where t0=dly*TB
    pset: = [type, ]	plot settings		pulse type from list
    					['man','rcf','rect','rrcf','sinc','tri']

    """
    if pset[0] == 'bn':
         print('Transmitted {:s}: \r\n '.format(pset[1], ), sig_xx.sig)
    elif pset[0] == 'bt':
        figure()
        plot(sig_xx.timeAxis(), sig_xx.sig), grid()
        title('{:s}'.format(pset[1]))
        xlabel('Time [s]')
        if pset[2] != 0:
            xlim([0, pset[2]])
        show()
    return

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def askrcvr(sig_rt,rtype,fcparms,FBparms,ptype,pparms):
    """
    Amplitude Shift Keying (ASK) Receiver for
    Coherent ('coh') and Non-coherent ('noncoh') ASK Signals
    >>>>> sig_bn,sig_bt,sig_wt,ixn =
    askrcvr(sig_rt,rtype,fcparms,FBparms,ptype,pparms) <<<<<
    where
    sig_bn: 			sequence from class sigSequ
    sig_bn.signal(): 	received DT sequence b[n]
    sig_bt: 			waveform from class sigWave
    sig_bt.signal(): 	received 'CT' PAM signal b(t)
    sig_wt: 			waveform from class sigWave
    sig_wt.signal(): 	wt = wit + 1j*wqt
    wit:				in-phase component of b(t)
    wqt:				quadrature component of b(t)
    ixn:				sampling time indexes for b(t)->b[n], w(t)->w[n]
    sig_rt: 			waveform from class sigWave
    sig_rt.signal():	received (noisy) ASK signal r(t)
    sig_rt.timeAxis(): 	time axis for r(t)
    rtype:				receiver type from list ['coh','noncoh']
    fcparms = [fc, thetac] 	for {'coh'}
    fcparms = [fc]			for {'noncoh'}
    fc:					carrier frequency in Hz
    thetac:				carrier phase in deg (0: cos, -90: sin)
    FBparms = [FB, dly]
    FB:					baud rate of PAM signal, TB=1/FB
    dly:				sampling delay for b(t)->b[n], fraction of TB
    					sampling times are t=n*TB+t0 where t0=dly*TB
    ptype:				pulse type from list
    					['man','rcf','rect','rrcf','sinc','tri']
    pparms = [] 		for 'man','rect','tri'
    pparms = [k, alpha] for {'rcf','rrcf'}
    pparms = [k, beta]	for {'sinc'}
    k:					"tail" truncation parameter for {'rcf','rrcf','sinc'}
    					(truncates at -k*TB and k*TB)
    alpha:				Rolloff parameter for {'rcf','rrcf'}, 0<=alpha<=1
    beta:				Kaiser window parameter for {'sinc'}
    """

    # ***** Get Carrier Frequency *****
    fc = fcparms[0]
    if rtype == 'coh':
        thetac = fcparms[1]

    # ***** Multiply by oscillator *****
    if rtype == 'noncoh':
        sig_vit = sdr.sigWave(sig_rt.sig*2*cos(2*pi*fc*sig_rt.timeAxis()),sig_rt.get_Fs(), sig_rt.get_t0())
        sig_vqt = sdr.sigWave(sig_rt.sig*-2*sin(2*pi*fc*sig_rt.timeAxis()),sig_rt.get_Fs(), sig_rt.get_t0())
    else:	# coh
        sig_vit = sdr.sigWave(sig_rt.sig*2*cos(2*pi*fc*sig_rt.timeAxis() + thetac), sig_rt.get_Fs(), sig_rt.get_t0())
        sig_vqt = sdr.sigWave(sig_rt.sig*-2*sin(2*pi*fc*sig_rt.timeAxis() + thetac), sig_rt.get_Fs(), sig_rt.get_t0())

    # ***** Filter r(t) with matched filter h_R(t) *****
    if rtype == 'noncoh':
        [sig_win, sig_wit, ixni] = sdr.pamrcvr10(sig_vit, FBparms, ptype, [0, 0], pparms)	  # Matched filter b(t)=vi(t)*h_R(t)
        [sig_wqt, sig_wqt, ixnq] = sdr.pamrcvr10(sig_vqt, FBparms, ptype, [0, 0], pparms)       # Matched filter b(t)=vq(t)*h_R(t)

        sig_bt = sdr.sigWave((np.sqrt(np.square(sig_wit.sig) + np.square(sig_wqt.sig))), sig_wit.get_Fs(), sig_wit.get_t0())
        sig_wt = sdr.sigWave((sig_wit.sig + 1j*sig_wqt.sig), sig_wit.get_Fs(), sig_wit.get_t0())  # Calculate wt for output
        # ***** Sample b(t) at t=n*TB+t0 to obtain b_n *****
        sig_bn = sdr.sigSequ(sig_bt.sig[ixni], FBparms[0], 0)                   # DT sequence sampled at t=n*TB+t0
        # TODO figure out what to do about n0 here
        ixn = ixni
    else: # coh
        [sig_bn, sig_bt, ixn] = sdr.pamrcvr10(sig_vit, FBparms, ptype, [0, 0], pparms)         # Matched filter b(t)=vi(t)*h_R(t)
        [sig_wqt, sig_wqt, ixnq] = sdr.pamrcvr10(sig_vqt, FBparms, ptype, [0, 0], pparms)       # Matched filter b(t)=vq(t)*h_R(t)
        sig_wt = sdr.sigWave((sig_bt.sig + 1j*sig_vqt.sig), sig_bt.get_Fs(), sig_bt.get_t0())

    return sig_bn, sig_bt, sig_wt, ixn

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def askrcvr_plot(sig_xx, pset, ix):
    """
    Amplitude Shift Keying (ASK) Receiver for
    Coherent ('coh') and Non-coherent ('noncoh') ASK Signals
    >>>>> sig_bn,sig_bt,sig_wt,ixn =
    askrcvr(sig_rt,rtype,fcparms,FBparms,ptype,pparms) <<<<<
    where
    sig_bn: 			sequence from class sigSequ
    sig_bn.signal(): 	received DT sequence b[n]
    sig_bt: 			waveform from class sigWave
    sig_bt.signal(): 	received 'CT' PAM signal b(t)
    sig_wt: 			waveform from class sigWave
    sig_wt.signal(): 	wt = wit + 1j*wqt
    wit:				in-phase component of b(t)
    wqt:				quadrature component of b(t)

    fcparms = [fc, thetac] 	for {'coh'}
    fcparms = [fc]			for {'noncoh'}
    fc:					carrier frequency in Hz
    thetac:				carrier phase in deg (0: cos, -90: sin)
    FBparms = [FB, dly]
    FB:					baud rate of PAM signal, TB=1/FB
    dly:				sampling delay for b(t)->b[n], fraction of TB
    					sampling times are t=n*TB+t0 where t0=dly*TB
    pset: = [type, ]	plot settings		pulse type from list
    					['man','rcf','rect','rrcf','sinc','tri']

    """
    if pset[0] == 'bn':
         print('Received {:s}: Binary Sequence\r\n '.format(pset[1], ), sig_xx.sig)
    elif pset[0] == 'bt':
        figure()
        plot(sig_xx.timeAxis(), sig_xx.sig), grid()
        title('{:s} Time Series'.format(pset[1]))
        xlabel('Time [s]')
        if pset[2] != 0:
            xlim([0, pset[2]])
        show()
    elif pset[0] == 'wt':
        figure()
        plot(sig_xx.timeAxis(), sig_xx.sig), grid()
        title('Received Complex {:s} Time Series'.format(pset[1]))
        xlabel('Time [s]')
        if pset[2] != 0:
            xlim([0, pset[2]])
        show()
    elif pset[0] == 'wn':
        figure()
        twn = sig_xx.timeAxis()
        wn = sig_xx.sig[ix]
        wni = real(wn)
        wnq = imag(wn)
        plot(wni, wnq, 'ro'), grid()
        title('Scatter Plot for: {:s}'.format(pset[1]))
        xlabel('In-Phase w_{i}[n]')
        ylabel('Quadrature w_{q}[n]')
        if pset[2] != 0:
            xlim([0, pset[2]])
        show()
    return

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def fskxmtr(M,sig_dn,Fs,ptype,pparms,xtype,fcparms):
	"""
	M-ary Frequency Shift Keying (FSK) Transmitter for
	Choherent ('coh'), Non-coherent ('noncoh'), and
	Continuous Phase ('cpfsk') FSK Signals
	>>>>> sig_xt = fskxmtr(M,sig_dn,Fs,ptype,pparms,xtype,fcparms) <<<<<
	where
	sig_xt: 			waveform from class sigWave
	sig_xt.signal():	transmitted FSK signal, sampling rate Fs
	sig_xt.timeAxis(): 	time axis for x(t), starts at t=-TB/2
	M:					number of distinct symbol values in d[n]
	xtype:				Transmitter type from set {'coh','noncoh','cpfsk'}
	sig_dn: 			sequence from class sigSequ
	sig_dn.signal() 	= [dn] for ['coh','cpfsk']
	sig_dn.signal() 	= [[dn],[thetacn]] for ['noncoh']
	dn:					M-ary (0,1,..,M-1) N-symbol DT input sequence d_n
	thetacn: 			N-symbol DT sequence theta_c[n] in degrees,
						used instead of thetac0..thetacM-1 for {'noncoh'} FSK
	sig_dn.get_FB(): 	baud rate of d_n (and theta_c[n]), TB=1/FB
	Fs:					sampling rate of x(t)
	ptype:				pulse type from set
						{'man','rcf','rect','rrcf','sinc','tri'}
	pparms = [] 		for {'man','rect','tri'}
	pparms = [k alpha] 	for {'rcf','rrcf'}
	pparms = [k beta]	for {'sinc'}
	k:					"tail" truncation parameter for {'rcf','rrcf','sinc'}
						(truncates p(t) to -k*TB <= t < k*TB)
	alpha:				Rolloff parameter for {'rcf','rrcf'}, 0<=alpha<=1
	beta:				Kaiser window parameter for {'sinc'}
	fcparms = [[fc0,fc1,...,fcM-1],[thetac0,thetac1,...,thetacM-1]] for {'coh'}
	fcparms = [fc0,fc1,...,fcM-1] for {'noncoh'}
	fcparms = [fc, deltaf] for {'cpfsk'}
	fc0,fc1,...,fcM-1: 	FSK (carrier) frequencies for {'coh','noncoh'}
	thetac0,thetac1,...,thetacM-1: FSK (carrier) phases in deg
						(0: cos, -90: sin) for {'coh'}
	fc:					carrier frequency for {'cpfsk'}
	deltaf: 			frequency spacing for {'cpfsk'}
						for dn=0 -> fc, dn=1 -> fc+deltaf,
						dn=2 -> fc+2*deltaf, etc
	"""
	# ***** Get signal *****
	dn = sig_dn.sig
	FB = sig_dn.get_FB()
	# txt = sig_dn.timeAxis()

	# ***** Get fc data *****
	if xtype == 'coh':
		fcn = fcparms[0]
		thetac = fcparms[1]
	elif xtype == 'noncoh':
		fcn = fcparms
	elif xtype == 'cpfsk':
		fc = fcparms[0]
		deltaf = fcparms[1]

	# ***** Demux binary (0,1) signals for each M level *****
	dM =  asarray([[0 for i in range(len(dn))] for j in range(M)])
	for n in range(0, M):
		ix = where(dn == n)
		dM[n,:][ix] = 1
		# print('d_{:d}: '.format(n), dM[n,:])

	# ***** Run dn through PAM *****
	if xtype == 'coh':
		xt = zeros(int(Fs*len(dn)/FB))
		for n in range(0, M):	# TODO be great to do this without a loop but python is weird
			sm = sdr.pam12_2(sdr.sigSequ(dM[n,:], FB, 0), Fs, ptype, 0, pparms)
			tsm = sm.timeAxis()
			xm = sm.sig*cos(2*pi*fcn[n]*tsm + pi/180.0*thetac[n])
			# Sum the results
			xt = xt + xm
	elif xtype == 'noncoh':
		xt = zeros(int(Fs*len(dn)/FB))
		thetacn = 2*pi*random(len(dn))	# TODO random phase for every M level or every value in every level?
		for n in range(0, M):
			sm = sdr.pam12_2(sdr.sigSequ(dM[n,:], FB, 0), Fs, ptype, 0, pparms)
			tsm = sm.timeAxis()
			xm = sm.sig*cos(2*pi*fcn[n]*tsm + pi/180.0*thetacn[n])
			# Sum the results
			xt = xt + xm
	elif xtype == 'cpfsk':
		xm = zeros(int(Fs*len(dn)/FB))
		sm = sdr.pam12_2(sdr.sigSequ(dM[n,:], FB, 0), Fs, ptype, 0, pparms)
		tsm = sm.timeAxis()
		phim = 2*pi*cumsum(sm.sig)*1/Fs
		xt = cos(2*pi*fc*tsm + deltaf*phim)

	return sdr.sigWave(xt, Fs, sm.get_t0())
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def fskxmtr_plot(M, sig_xt,FB,xtype,pset):
	"""
	M-ary Frequency Shift Keying (FSK) Transmitter for
	Choherent ('coh'), Non-coherent ('noncoh'), and
	Continuous Phase ('cpfsk') FSK Signals
	>>>>> sig_xt = fskxmtr(M,sig_dn,Fs,ptype,pparms,xtype,fcparms) <<<<<
	where
	sig_xt: 			waveform from class sigWave
	sig_xt.signal():	transmitted FSK signal, sampling rate Fs
	sig_xt.timeAxis(): 	time axis for x(t), starts at t=-TB/2
	M:					number of distinct symbol values in d[n]
	xtype:				Transmitter type from set {'coh','noncoh','cpfsk'}
	thetacn: 			N-symbol DT sequence theta_c[n] in degrees,
						used instead of thetac0..thetacM-1 for {'noncoh'} FSK
	sig_dn.get_FB(): 	baud rate of d_n (and theta_c[n]), TB=1/FB
	Fs:					sampling rate of x(t)
	FB: 				BAUD rate of x(t)
	ptype:				pulse type from set
						{'man','rcf','rect','rrcf','sinc','tri'}
	pparms = [] 		for {'man','rect','tri'}
	pparms = [k alpha] 	for {'rcf','rrcf'}
	pparms = [k beta]	for {'sinc'}

	fcparms = [[fc0,fc1,...,fcM-1],[thetac0,thetac1,...,thetacM-1]] for {'coh'}
	fcparms = [fc0,fc1,...,fcM-1] for {'noncoh'}
	fcparms = [fc, deltaf] for {'cpfsk'}
	fc0,fc1,...,fcM-1: 	FSK (carrier) frequencies for {'coh','noncoh'}
	thetac0,thetac1,...,thetacM-1: FSK (carrier) phases in deg
						(0: cos, -90: sin) for {'coh'}
	fc:					carrier frequency for {'cpfsk'}
	deltaf: 			frequency spacing for {'cpfsk'}
						for dn=0 -> fc, dn=1 -> fc+deltaf,
						dn=2 -> fc+2*deltaf, etc
	pset:				Plot settings [type, tlim]
						Type i.e. {'psd'} or {'ts'}(time series)
						tlim = time limit [s]
	"""
	# ***** Choose Plot Title *****
	xtype = xtype.lower() 	# Cap protection
	if xtype == 'coh':
		sigtype = 'Coherent FSK'
	elif xtype == 'noncoh':
		sigtype = 'Non-coherent FSK'
	elif xtype == 'cpfsk':
		sigtype = 'Continuous Phase FSK'

	if pset[0] == 'tx':
		figure()
		plot(sig_xt.timeAxis(), sig_xt.sig,'r'), grid()
		if pset[1] != 0:
			xlim([0, pset[1]])
		ylabel('x(t)')
		xlabel('t [s]')
		title('{:s} Signal x(t), FB = {:d}'.format(sigtype, FB))
	elif pset[0] == 'psd':
		ft.showpsd0(sig_xt, [-pset[1], pset[1], -60], sig_xt.get_Fs())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def fskrcvr(M,sig_rt,rtype,fcparms,FBparms,ptype,pparms):
	"""
	M-ary Frequency Shift Keying (FSK) Receiver for
	Coherent ('coh'), Non-coherent ('noncoh'), and
	Phase Detector ('phdet') FSK Reception
	>>>>> sig_bn,sig_wt,ixn =
	fskrcvr(M,sig_rt,rtype,fcparms,FBparms,ptype,pparms) <<<<<
	where
	sig_bn: 			sequence from class sigSequ
	sig_bn.signal(): 	received DT sequence b[n]
	sig_wt: 			waveform from class sigWave
	sig_wt.signal(): 	wt = [[w0it+1j*w0qt],[w1it+1j*w1qt],...,
						[wM-1it+1j*wM-1qt]]
	wmit:				m-th in-phase matched filter output
	wmqt:				m-th quadrature matched filter output
	ixn:				sampling time indexes for b(t)->b[n], w(t)->w[n]
	M:					number of distinct FSK frequencies
	sig_rt: 			waveform from class sigwave
	sig_rt.signal():	received (noisy) FSK signal r(t)
	sig_rt.timeAxis(): 	time axis for r(t)
	rtype:				receiver type from list {'coh','noncoh','phdet'}
	fcparms = [[fc0,fc1,...,fcM-1],[thetac0,thetac1,...,thetacM-1]]
						for {'coh'}
	fcparms = [fc0,fc1,...,fcM-1]
						for {'noncoh'}
	fcparms = [fc, deltaf]
	 					for {'phdet'}
	fc0,fc1,...,fcM-1:	FSK (carrier) frequencies
						for {'coh','noncoh'}
	thetac0,thetac1,...,thetacM-1: FSK (carrier) phases in deg
						(0: cos, -90: sin) for {'coh'}
	fc:					carrier frequency for {'cpfsk'}
	deltaf: 			frequency spacing for {'cpfsk'}
						for dn=0 -> fc, dn=1 -> fc+deltaf,
	dn=2 -> fc+2*deltaf, etc
	FBparms = [FB, dly]
	FB:					baud rate of PAM signal, TB=1/FB
	dly:				sampling delay for wm(t)->wm[n], fraction of TB
						sampling times are t=n*TB+t0 where t0=dly*TB
	ptype:				pulse type from list
						{'man','rcf','rect','rrcf','sinc','tri'}
	pparms = [] 		for {'man','rect','tri'}
	pparms = [k, alpha] for {'rcf','rrcf'}
	pparms = [k, beta]	for {'sinc'}
	k:					"tail" truncation parameter for {'rcf','rrcf','sinc'}
						(truncates at -k*TB and k*TB)
	alpha:				Rolloff parameter for {'rcf','rrcf'}, 0<=alpha<=1
	beta:				Kaiser window parameter for {'sinc'}
	"""
	# ***** Get signal *****
	rt = sig_rt.sig
	Fs = sig_rt.get_Fs()
	t0 = sig_rt.get_t0()
	trt = sig_rt.timeAxis()
	FB = FBparms[0]
	dly = FBparms[1]

	# ***** Get fc data *****
	if rtype == 'coh':
		fcn = fcparms[0]
		thetac = fcparms[1]
	elif rtype == 'noncoh':
		fcn = fcparms
	elif rtype == 'cpfsk':
		fc = fcparms[0]
		deltaf = fcparms[1]

	# ***** Demux binary (0,1) signals for each M level *****
	if rtype == 'coh':
		# Put on oscillator and match filter
		vm = rt*2*cos(2*pi*fcn[0]*trt + pi/180.0*thetac[0])
		[tmp_bn, tmp_bt, ixn] = sdr.pamrcvr10(sdr.sigWave(vm, Fs, t0), FBparms, ptype, [0, 0], pparms)



		wm = tmp_bt.signal()
		for n in range(1, M):
			vm = rt*2*cos(2*pi*fcn[n]*trt + pi/180.0*thetac[n])
			[tmp_bn, tmp_bt, ixn] = sdr.pamrcvr10(sdr.sigWave(vm, Fs, t0), FBparms, ptype, [0, 0], pparms)
			wm = np.vstack((wm, tmp_bt.signal()))

		wt = wm
	elif rtype == 'noncoh':
		# Put on oscillator and match filter
		vmi = rt*2*cos(2*pi*fcn[0]*trt)
		vmq= rt*-2*sin(2*pi*fcn[0]*trt)
		[tmp_bni, tmp_bti, ixni] = sdr.pamrcvr10(sdr.sigWave(vmi, Fs, t0), FBparms, ptype, [0, 0], pparms)
		wmi = tmp_bti.signal()
		[tmp_bnq, tmp_btq, ixnq] = sdr.pamrcvr10(sdr.sigWave(vmq, Fs, t0), FBparms, ptype, [0, 0], pparms)
		wmq = tmp_btq.signal()

		wt = wmi + 1j*wmq;

		wm = np.sqrt(np.square(wmi) + np.square(wmq))
		for n in range(1, M):
			vmi = rt*2*cos(2*pi*fcn[n]*trt)
			vmq= rt*-2*sin(2*pi*fcn[n]*trt)
			[tmp_bni, tmp_bti, ixni] = sdr.pamrcvr10(sdr.sigWave(vmi, Fs, t0), FBparms, ptype, [0, 0], pparms)
			wmi = tmp_bti.signal()
			[tmp_bnq, tmp_btq, ixnq] = sdr.pamrcvr10(sdr.sigWave(vmq, Fs, t0), FBparms, ptype, [0, 0], pparms)
			wmq = tmp_btq.signal()

			wt = np.vstack((wt, wmi + 1j*wmq))

			wm = np.vstack((wm, np.sqrt(np.square(wmi) + np.square(wmq))))
		ixn = ixni
	elif rtype == 'cpfsk':
		print('not written yet')
		# phase detector, instantaneous frequency is the derivative of the phase
		# see lab 8
		# remember to divide by deltaf

	# ***** Select largest index to extrapolate correct signal *****
	# wt should be Mleveled sigWave of wi + 1jwq
	sig_wt = sdr.sigWave(wt, Fs, t0)
	bn = wm.argmax(axis = 0) # this should be bn
	sig_bn = sdr.sigWave(bn[ixn], Fs, t0)
	# sig_bn = sdr.sigSequ(sig_wt.sig[ixn], FB, 0)

	return sig_bn, sig_wt, ixn
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def fskrcvr_plot(M,  sig_xx, pset, ix):
	"""
	Amplitude Shift Keying (ASK) Receiver for
	Coherent ('coh') and Non-coherent ('noncoh') ASK Signals
	>>>>> sig_bn,sig_bt,sig_wt,ixn =
	askrcvr(sig_rt,rtype,fcparms,FBparms,ptype,pparms) <<<<<
	where
	sig_bn: 			sequence from class sigSequ
	sig_bn.signal(): 	received DT sequence b[n]
	sig_bt: 			waveform from class sigWave
	sig_bt.signal(): 	received 'CT' PAM signal b(t)
	sig_wt: 			waveform from class sigWave
	sig_wt.signal(): 	wt = wit + 1j*wqt
	wit:				in-phase component of b(t)
	wqt:				quadrature component of b(t)

	fcparms = [fc, thetac] 	for {'coh'}
	fcparms = [fc]			for {'noncoh'}
	fc:					carrier frequency in Hz
	thetac:				carrier phase in deg (0: cos, -90: sin)
	FBparms = [FB, dly]
	FB:					baud rate of PAM signal, TB=1/FB
	dly:				sampling delay for b(t)->b[n], fraction of TB
						sampling times are t=n*TB+t0 where t0=dly*TB
	pset: = [type, ]	plot settings		pulse type from list
						['man','rcf','rect','rrcf','sinc','tri']

	"""
	if pset[0] == 'bn':
		print('Received {:s}: Binary Sequence\r\n '.format(pset[1], ), sig_xx.sig)
	elif pset[0] == 'bt':
		figure()
		plot(sig_xx.timeAxis(), sig_xx.sig), grid()
		title('{:s} Time Series'.format(pset[1]))
		xlabel('Time [s]')
		if pset[2] != 0:
			xlim([0, pset[2]])
		show()
	elif pset[0] == 'wt':
		figure()
		wt = sig_xx.signal()
		for i in range(0,M):
			plot(sig_xx.timeAxis(), sig_xx.signal()[i,:])
			title('Received Complex {:s} Time Series'.format(pset[1]))
			xlabel('Time [s]')
		if pset[2] != 0:
			xlim([0, pset[2]])
		grid()
		show()
	elif pset[0] == 'wn':
		figure()
		twt = sig_xx.timeAxis()
		# wt = sig_xx.sig[ix]
		wt = sig_xx.sig
		# string = ('0')
		for i in range(0,M):
			wti_i = real(wt[i,:][ix])
			wtq_i = imag(wt[i,:][ix])
			plot(wti_i, wtq_i, 'o', label=str(i))
			title('Scatter Plot for: {:s}'.format(pset[1]))
			xlabel('In-Phase w_{i}[n]')
			ylabel('Quadrature w_{q}[n]')
			# string = np.hstack((string, str(i)))
		if pset[2] != 0:
			xlim([0, pset[2]])
		legend(loc='upper left')
		grid()
		show()
	return

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

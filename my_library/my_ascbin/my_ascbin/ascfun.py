# File: ascfun.py
# Functions for conversion between ASCII and bits
from pylab import *

def asc2bin(txt, bits=8):
	"""
	ASCII text to serial binary conversion
	>>>>> dn = asc2bin(txt, bits) <<<<<
	where	txt			input text string
			abs(bits) 	bits per char, default=8
			bits > 0	LSB first parallel to serial
			bits < 0	MSB first parallel to serial
			dn			binary output sequence
	"""
	txtnum = array([ord(c) for c in txt]) # int array

	if bits > 0:	# Neg powers of 2, increasing exp
		p2 = np.power(2.0, arange(0,-bits,-1))
	else:			# Neg powers of 2, decreasing exp
		p2 = np.power(2.0, 1+arange(bits,0))

	B = array(mod(array(floor(outer(txtnum,p2)),int),2),int8)	# Rows of B are bits of chars
	dn = reshape(B,B.size)
	return dn	 # Serial binary output
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def bin2asc(dn, bits=8, flg=1):
	"""
	Serial binary to ASCII text conversion
	>>>>> txt = bin2asc(dn, bits, flg) <<<<<
	where	dn			binary input sequence
			abs(bits) 	bits per char, default=8
			bits > 0	LSB first parallel to serial
			bits < 0	MSB first parallel to serial
			flg != 0	limit range to [0...127]
			txt			output text string
	"""

	numBits = 8
	numChar = int(len(dn)/numBits)
	my_string_dec = zeros(numChar)
	for i in range(0, numChar):
	    for j in range(0,numBits):
	        index = i*numBits + j
	        my_string_dec[i] += dn[index]*pow(2, j)

	# print("Binary String: ", my_string_dec)
	txt = '\0'   # Reset this to null every time
	for i in range(0, numChar):
	    txt += chr(int(my_string_dec[i])) # Only way I could get this to work

	return txt
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def binthresh(dn, bits=8, numL=1):
	"""
	Convert filtered binary data to crisp 1's and 0's
	>>>>> txt = bin2asc(dn, bits, flg) <<<<<
	where	dn			binary input sequence
			abs(bits) 	bits per char, default=8
			bits > 0	LSB first parallel to serial
			bits < 0	MSB first parallel to serial
			numL		Number of levels in sequence
			bn			thresheld binary output sequence
	"""
	bn = arange(0, len(dn))
	ixp = where(dn > 0)[0]
	ixn = where(dn < 0)[0]
	bn[ixp] = 1
	bn[ixn] = 0

	return bn

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def binthresh_2(dn, threshold, bits=8, numL=1):
	"""
	Convert filtered binary data to crisp 1's and 0's
	>>>>> txt = bin2asc(dn, bits, flg) <<<<<
	where	dn			binary input sequence
			abs(bits) 	bits per char, default=8
			bits > 0	LSB first parallel to serial
			bits < 0	MSB first parallel to serial
			numL		Number of levels in sequence
			bn			thresheld binary output sequence
	"""
	bn = arange(0, len(dn))
	ixp = where(dn >= threshold)[0]
	ixn = where(dn < threshold)[0]
	bn[ixp] = 1
	bn[ixn] = 0

	return bn

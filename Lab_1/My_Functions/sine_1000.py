# File: sine_1000.py
# Generates 1.5 sec of 1000 Hz sinusoidal waveform
from pylab import *
# Parameters
Fs = 8000
fm = 1000
tlen = 1.5
# Generate time axis
tt = arange(0,round(tlen*Fs))/float(Fs)
# Generate sinewave
xt = sin(2*pi*fm*tt)


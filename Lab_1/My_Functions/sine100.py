# File: sine100.py
# Asks for sampling frequency Fs and then generates
# and plots 5 periods of a 100 Hz sinewave
from pylab import *
from ast import literal_eval

def sine100(Fs):
    # Fs = literal_eval(input('Enter sampling rate Fs in Hz: '))
    f0 = 100 # Frequency of sine
    tlen = 5e-2   # Signal duration in sec
    tt = arange(0,round(tlen*Fs))/float(Fs) # Time axis
    st = sin(2*pi*f0*tt) # Sinewave, frequency f0
    return[tt, st]

def sineGen(Fs, fm, tlen):
	"""
	Generate sinusoid based on inputs
	>>>>> tt, mt = sineGen(Fs, fm, tlen) <<<<<
	where	Fs		Sampling Frequency
			fm 		Sine Frequency
			tlen	Time Interval
			tt		Time axis
			mt		Sinewave
	"""
	# Generate time axis
	tt = arange(0,round(tlen*Fs))/float(Fs)
	# Generate sinewave
	mt = sin(2*pi*fm*tt)
	return tt, mt

# File: ftpam_rcvr01.py
# Script file that accepts a binary unipolar flat-top PAM
# signal r(t) with bitrate Fb and sampling rate Fs as
# input and decodes it into a received text string.
# The PAM signal r(t) is received from a wav-file with
# sampling rate Fs. First r(t) is sampled at the right
# DT sequence sampling times, spaced Tb = 1/Fb apart. The
# result is then quantized to binary (0 or 1) to form the
# estimated received sequence dnhat which is subsequently
# converted to 8-bit ASCII text.

from pylab import *
import ascfun as af
import wavfun as wf
from ascfun import bin2asc

def wav2bin(filename, Fb):
	[rt, Fs] = wf.wavread(filename)
	# plot(arange(0,len(rt)), rt)
	num2avg = 5 # Define number of samples to average over
	print("# of Samples:", len(rt))
	if Fb == 0:		# Then we need to determine the bit rate
		flag = 0
		i = 5
		while(flag < 2):
			rtLast = mean(rt[i-num2avg:i])
			# print("rtLast = ", rtLast)
			rtPrev = mean(rt[i:i+num2avg])
			# print("rtPrev = ", rtPrev)
			if abs(rtLast - rtPrev) >= .5:
				if flag == 0:
					index1 = i 		# Store index of first threshold
					# print("index1 = ",index1)
					flag += 1 		# Break loop, condition met
					i += num2avg	# Move index forward atleast length of num2avg to avoid immediately detecting a drop again
				elif flag == 1:
					index2 = i
					# print("index2 = ",index2)
					flag += 1 		# Break loop, condition met
					i += num2avg	# Move index forward atleast length of num2avgto avoid immediately detecting a drop again
			if i > (len(rt) - num2avg):	# Algorithm has failed to determine bit rate
				flag = 2 			# Force exit of loop
				print("Failure to determine threshold")
				return
			i += 1
		numTpBit = index2 - index1  	# Have found # samples/bit
		print("#bits/Sample: ", numTpBit, "  (Determined by wav2ascii())")
	else:		# Bit rate was provided with function call
		Tb = 1/float(Fb)				# Time per bit
		bits = 8						# Number of bits/char
		N = floor(len(rt)/float(Fs)/Tb) # Number of received bits
		numTpBit = int(len(rt)/N)     	# Number of samples per bit

	# >>Sample and quantize the received PAM signal here<<
	dnhat = zeros(int(len(rt)/numTpBit)) 	# Create array to hold dnhat
	print('Bytes in File: ', len(dnhat)/8)

	index = int(numTpBit/2)				# Set up index for dnhat
	print("numTpBit: ", numTpBit)
	# Redefine num2avg to fit within numTpBit so as to avoid accessing inaccessible memory
	num2avg = int(.5*(numTpBit/2))
	print("num2avg: ", num2avg)

	for i in range (0, len(dnhat)-1): 	# Loop over array r(t) by numTpBit
		dnhat[i] = round(mean(rt[index-num2avg:index+num2avg]))     # Populate s(t)
		index += numTpBit
	print("# of Bits: ", len(dnhat))
	return dnhat

def wav2ascii(filename, Fb):
	dnhat = wav2bin(filename, Fb)			# Convert wavform to binary
	txthat = bin2asc(dnhat)					# Convert binary string to ASCII
	print("ASCII txt    : ", txthat)		# Print result

	return txthat

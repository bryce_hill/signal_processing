#!/bin/bash

#
echo "Uninstalling Bryce Hill's python packages in develop mode"
echo "Packages can be installed by running the: "
echo "install_bryce_lib.sh script"

cd my_library/my_grc
sudo python setup.py develop -u
sudo python3 setup.py develop -u

cd ../my_ascbin
sudo python setup.py develop -u
sudo python3 setup.py develop -u

cd ../my_ft
sudo python setup.py develop -u
sudo python3 setup.py develop -u

cd ../my_sinecos
sudo python setup.py develop -u
sudo python3 setup.py develop -u

cd ../my_sdr
sudo python setup.py develop -u
sudo python3 setup.py develop -u

cd ../my_jupyter
sudo python setup.py develop -u
sudo python3 setup.py develop -u

cd ../my_am
sudo python setup.py develop -u
sudo python3 setup.py develop -u

cd ../my_shiftKeying
sudo python setup.py develop -u
sudo python3 setup.py develop -u

#!/usr/bin/env Python
# Script to find python package locations
# Taken from: https://stackoverflow.com/questions/22543745/cant-import-numpy


import os
import sys
for p in sys.path:
	if os.path.exists(os.path.join(p, 'numpy')):
		print p
		break
	else:
		print "Numpy not found"

